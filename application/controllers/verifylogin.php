<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class VerifyLogin extends CI_Controller {

  public $layout = 'default-login';

  public $css = array('materialize/materialize.min', 'login');

  public $js = array('jquery-2.1.1.min', 'materialize.min');
 
 function __construct()
 {
   parent::__construct();
   $this->load->model('user','',TRUE);
   $this->load->library('form_validation');
   $this->load->helper(array('form', 'url', 'array', 'date'));
 }
 
 function index()
 {
   $this->load->library('form_validation');
   //This method will have the credentials validation
 
   $this->form_validation->set_rules('username', 'E-mail', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Senha', 'trim|required|xss_clean|callback_check_database');
 
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('login_view');
   }
   else
   {
     //Go to private area
     redirect('home', 'refresh');
   }
 
 }
 function recovery()
 {
   //This method will have the credentials validation
   $this->load->library('form_validation');
 
   $this->form_validation->set_rules('email', 'E-mail', 'trim|required|xss_clean|callback_check_email');
 
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('recovery-pass');
   }
   else
   {
     //Go to private area
     redirect('send-pass', 'refresh');
   }
 
 }


 function check_database($password)
 {
   //Field validation succeeded.  Validate against database
   $username = $this->input->post('username');
 
   //query the database
   $result = $this->user->login($username, $password);
 
   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
         'id' => $row->id,
         'username' => $row->email
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     return TRUE;
   }
   else
   {
     $this->form_validation->set_message('check_database', 'Usuário e Senha incorreto');
     return false;
   }
 }
 function check_email()
 {
   //Field validation succeeded.  Validate against database
   $email = $this->input->post('email');
   $this->load->library('email');
 
   //query the database
   $result = $this->user->recovery($email);
 
   if($result)
   {
     $sess_array = array();
     foreach($result as $row)
     {
       $sess_array = array(
          'nome' => $row->nome,
          'email' => $row->email
       );
       $this->session->set_userdata('logged_in', $sess_array);
     }
     //return TRUE;
     // Envia email com usuário e nova senha para o e-mail cadastrado.
      $data = array('id' => $row->id,'nome' => $row->nome,'usuario' => $row->usuario,'email' => $row->email,'senha' => $row->senha );

      //Gera uma nova senha
      $novasenha = date('sms').$data['id'].substr($data['usuario'], -2, 1).substr($data['nome'], -2);

      //Criptografa a nova senha
      $data['senha'] = $this->user->senha = md5($novasenha);

      //Grava a nova senha criptografada
      $this->user->trocar_senha($data);

      $nome = $data['nome'];
      $email = $data['email'];
      $usuario = $data['usuario'];
      $senha = $novasenha;

      //Faz o envio do e-mail
      
      $this->email->set_mailtype("html");

      $this->email->from($email, 'Recuperar Senha');
      $this->email->to($email); 
      //$this->email->cc('outro@outro-site.com'); 
      //$this->email->bcc('dev@vetorart.com.br'); 

        // Monta a mensagem HTML que será enviada.
        $mensagem = "Nome: ".$nome."<br><br>Usuario: ".$usuario."<br><br>Nova Senha: ".$senha;

        $this->email->subject('Recuperação de Senha');
        $this->email->message($mensagem); 
        $this->email->send();

        //die($mensagem);  

        
        return TRUE;




     //die($novasenha);


   }
   else
   {
     $this->form_validation->set_message('check_email', 'E-mail não encontrado');
     return false;
   }
 }

}
?>