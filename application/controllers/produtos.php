<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Produtos extends CI_Controller {

  public $layout = 'default-admin';

  public $css = array('materialize/materialize.min', 'admin');

  public $js = array('jquery-2.1.1.min', 'materialize.min', 'filter-table');
 
 function __construct()
 {
   parent::__construct();
   //$this->load->model('noticias_model');
   $this->load->helper(array('form', 'url', 'array', 'date'));
   $this->load->library('form_validation', 'session');
 }
 
 /*BANNERS */
 function list_produtos()
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');

     $data = array('usuario' => $session_data['username'] );

     $this->load->model('produtos_model');
     $data['dados'] = $this->produtos_model->listaADM();

     $data['order'] = $this->produtos_model->orders();

     $this->load->view('admin/list-produtos', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 function cad_produto()
 {
   if($this->session->userdata('logged_in'))
   {
    // Carrega o Model
    $this->load->model('produtos_model');
    // Armazena infs de login
    $session_data = $this->session->userdata('logged_in');
    $data = array('usuario' => $session_data['username'] );
    // Captura id em caso de edição
    $id = $this->uri->segment(3, 0);
    // Consulta e puxa infs do banco através do model
    $data['dados'] = $this->produtos_model->editar($id);

    $this->css = array('materialize/materialize.min', 'summernote/materialSummernote', 'summernote/codeMirror/codemirror', 'summernote/codeMirror/monokai', 'admin');

    $this->js = array('jquery-2.1.1.min', 'materialize.min', 'filter-table', 'summernote/zzz_ckMaterializeOverrides', 'summernote/codeMirror/codemirror', 'summernote/codeMirror/xml', 'summernote/materialSummernote');

    // Carrega view
    $this->load->view('admin/form-produto', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }

 function ordenar()
 {
   if($this->session->userdata('logged_in'))
   {
    // Carrega o Model
    $this->load->model('produtos_model');
    // Armazena infs de login
    $session_data = $this->session->userdata('logged_in');
    $data = array('usuario' => $session_data['username'] );
    // Captura id em caso de edição
    $ordemAtual = $this->uri->segment(4, 0);
    $ordemPret = $this->uri->segment(5, 0);

    if ($ordemPret < $ordemAtual) {
      // busca somente o registro ATUAL no banco
      $produtoAtual = $this->produtos_model->prodAtual($ordemAtual);

      foreach ($produtoAtual as $pat) {
        $inicial['id'] = $pat->id;
        $inicial['ordem'] = $ordemPret;
      }

      $v1 = "produtos.ordem < ".$ordemAtual;
      $v2 = "produtos.ordem >= ".$ordemPret;
      //$inicial['ordem'] = $oap;
      //echo $orAtual." - ".$orPret;
        $valores = $this->produtos_model->atualizaOrdem($v1, $v2);
        //print_r($infs);
        foreach ($valores as $inf) {
            if($ordemPret == $ordemAtual)
               break;
            $dados['id'] = $inf->id;
            $dados['ordem'] = strval($inf->ordem +1);
            //echo $inf->nome." - ".$dados['id']." - ".$dados['ordem']."<br>";
            $this->produtos_model->reordenar($dados);
            $ordemPret++;
            $this->produtos_model->reordenar_inicial($inicial);
            //redirect('ctrl/produtos', 'refresh');         
        }   

    } else{
      // busca somente o registro ATUAL no banco
      $produtoAtual = $this->produtos_model->prodAtual($ordemAtual);

      foreach ($produtoAtual as $pat) {
        $inicial['id'] = $pat->id;
        $inicial['ordem'] = $ordemPret;
      }

      $v1 = "produtos.ordem <= ".$ordemPret;
      $v2 = "produtos.ordem > ".$ordemAtual;
      //$inicial['ordem'] = $oap;
      //echo $orAtual." - ".$orPret;
        $valores = $this->produtos_model->atualizaOrdem($v1, $v2);
        //print_r($infs);
        foreach ($valores as $inf) {
            if($ordemPret == $ordemAtual)
               break;
            $dados['id'] = $inf->id;
            $dados['ordem'] = strval($inf->ordem -1);
            //echo $inf->nome." - ".$dados['id']." - ".$dados['ordem']."<br>";
            $this->produtos_model->reordenar($dados);
            $ordemPret--;
            $this->produtos_model->reordenar_inicial($inicial);
            //redirect('ctrl/produtos', 'refresh');         
        }
    }
    redirect('ctrl/produtos', 'refresh');    
  }
    else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  } 
}
function deletar_produto() 
 {
    /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
    $this->load->helper('file');
    $id = $this->uri->segment(3, 0);
    $this->load->model('produtos_model');

    $data['dados'] = $this->produtos_model->editar($id);
    //echo "<pre>"; print_r($data['dados'][0]->img);die();
    $idProd = $data['dados'][0]->id;
    $ordem = $data['dados'][0]->ordem;

    $infs = $this->produtos_model->orders();
    // Conta o número de registros na base
    $totalRows = count($infs);

      if ($ordem < $totalRows) {
        $v1 = "produtos.ordem <= ".$totalRows;
        $v2 = "produtos.ordem > ".$ordem;

        $valores = $this->produtos_model->OrgDel($v1, $v2);

        foreach ($valores as $inf) {
            if($totalRows == $ordem)
               break;
            $dados['id'] = $inf->id;
            $dados['ordem'] = strval($inf->ordem -1);
            //echo $inf->nome." - ".$dados['id']." - ".$dados['ordem']."<br>";
            $this->produtos_model->reordenar($dados);
            $totalRows--;
        }
        if($this->produtos_model->deletar($id)) {
          $pasta = './images/produtos/';
          $arq = $data['dados'][0]->img;

          unlink($pasta.$arq);
          /* Caso sucesso ao atualizar, recarrega a página principal */
          $this->session->set_flashdata('sucesso', 'Produto deletado.');
          redirect('ctrl/produtos');
        }
      }

    if($this->produtos_model->deletar($id)) {
      $pasta = './images/produtos/';
      $arq = $data['dados'][0]->img;

      unlink($pasta.$arq);
      /* Caso sucesso ao atualizar, recarrega a página principal */
      $this->session->set_flashdata('sucesso', 'Produto deletado.');
      redirect('ctrl/produtos');
    } else {
      /* Senão exibe a mensagem de erro */
      $this->session->set_flashdata('erro', 'Não foi possível deletar o produto.');
      redirect('home');
    }
}
function salvar_produto()
{
  if($this->session->userdata('logged_in'))
  {
    $session_data = $this->session->userdata('logged_in');
    // Configs de upload
    $config['upload_path']          = './images/produtos/';
    $config['allowed_types']        = 'gif|jpg|png';
    //$config['max_size']             = 2000;
    //$config['max_width']            = 1200;
    //$config['max_height']           = 1200;
    $config['encrypt_name']         = TRUE;
    // Carrega library para upload de imagem
    $this->load->library('upload', $config);
    // Carrega model responsável
    $this->load->model('produtos_model');    
    //$this->load->model('banners_model');  
    // Consulta base de registros dos produtos
    $infs = $this->produtos_model->orders();
    // Conta o número de registros na base
    $count = count($infs);
    // Adiciona mais 1 para inserir na ordem do novo produto que deverá ser cadastrado em último.
    $contador = $count +1;

    $this->upload->do_upload();
    //Infs do formulário
    $data['id'] = $this->produtos_model->id = $this->input->post('id');
    $data['nome'] = $this->produtos_model->nome = $this->input->post('nome');
    $data['idcategoria'] = $this->produtos_model->idcategoria = $this->input->post('idcategoria');
    $data['video'] = $this->produtos_model->video = $this->input->post('video');
    $data['tags'] = $this->produtos_model->tags = $this->input->post('tags');
    $data['bdesc'] = $this->produtos_model->bdesc = $this->input->post('bdesc');
    $data['description'] = $this->produtos_model->description = $this->input->post('description');
    $data['descricao'] = $this->produtos_model->descricao = $this->input->post('descricao');
    $data['especificacao'] = $this->produtos_model->especificacao = $this->input->post('especificacao');
    $data['garantia'] = $this->produtos_model->garantia = $this->input->post('garantia');
    $img = $this->produtos_model->img = $this->upload->data();

    $slug = $data['nome'];
    $str = strtolower($slug);
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);

    $data['slug'] = str_replace(" ", "-", $str);

    if($img['file_name']!=""){
      $data['img'] = $img['file_name'];
    }

    $data['status'] = $this->produtos_model->status = $this->input->post('status');

    /*$data['error'] = 'Banner cadastrado com sucesso!';
    $data['username'] = $session_data['username'];*/

    if(intval($data['id'])<=0){
      $data['ordem'] = $contador;
      $this->produtos_model->inserirProduto($data);
    }
    else{
      $this->produtos_model->atualizar($data);
    }

    redirect('ctrl/produtos', 'refresh');

  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}

/* GALERIA */
function list_galeria()
  {
    if($this->session->userdata('logged_in')){
      $session_data = $this->session->userdata('logged_in');

      $this->js = array('jquery-2.1.1.min', 'materialize.min', 'filter-table');

      $data = array('usuario' => $session_data['username'] );
      $data = array('usuario' => $session_data['username'] );

      $this->load->model('galeria_model');
      $data['cardapio'] = $this->galeria_model->lista_cardapio();
      $data['cidade'] = $this->galeria_model->lista_cidade();

      $this->load->view('admin/list-galeria', $data);
    }else{
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
  }
function cad_galeria_cidade()
 {
   if($this->session->userdata('logged_in'))
   {
    $session_data = $this->session->userdata('logged_in');

    $id = $this->uri->segment(3, 0);
    $this->load->model('galeria_model');
    $this->load->model('cidades_model');

    $data['cidades'] = $this->cidades_model->lista();
    $data['dados'] = $this->galeria_model->editar_cidade($id);

    $this->load->view('admin/form-galeria-cidade', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
function upload_galeria_cidade()
{
  if($this->session->userdata('logged_in'))
  {
    $session_data = $this->session->userdata('logged_in');

    $config['upload_path']          = './images/galeria/';
    $config['allowed_types']        = 'gif|jpg|png';
    //$config['max_size']             = 2000;
    //$config['max_width']            = 1200;
    //$config['max_height']           = 1200;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);
    
    $this->load->model('galeria_model');    

    $this->upload->do_upload();

    $data['id'] = $this->galeria_model->id = $this->input->post('id');
    $data['idcidade'] = $this->galeria_model->idcidade = $this->input->post('idcidade');
    $data['titulo'] = $this->galeria_model->titulo = $this->input->post('titulo');
    $img = $this->galeria_model->img = $this->upload->data();
    $data['status'] = $this->galeria_model->status = $this->input->post('status');

    if($img['file_name']!=""){
      $data['img'] = $img['file_name'];
    }

    /*$data['error'] = 'Banner cadastrado com sucesso!';
    $data['username'] = $session_data['username'];*/

    if(intval($data['id'])<=0){
      $this->galeria_model->insert_cidade($data);
    }
    else{
      $this->galeria_model->atualizar_cidade($data);
    }

    redirect('ctrl/galeria', 'refresh');

  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}
function deletar_galeria_cidade() 
 {
    /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
    $this->load->helper('file');
    $id = $this->uri->segment(3, 0);
    $this->load->model('galeria_model');

    $data['dados'] = $this->galeria_model->editar_cidade($id);
    //echo "<pre>"; print_r($data['dados'][0]->img);die();

    $this->load->model('galeria_model');

    //die($id);
    if($this->galeria_model->deletar_cidade($id)) {
      $pasta = './images/galeria/';
      $arq = $data['dados'][0]->img;
      unlink($pasta.$arq);
      /* Caso sucesso ao atualizar, recarrega a página principal */
      $this->session->set_flashdata('sucesso', 'Banner deletado.');
      redirect('ctrl/galeria');
    } else {
      /* Senão exibe a mensagem de erro */
      $this->session->set_flashdata('erro', 'Não foi possível deletar o banner.');
      redirect('home');
    }
}

/* GALERIA CARDÁPIO */
function cad_galeria_cardapio()
 {
   if($this->session->userdata('logged_in'))
   {
    $session_data = $this->session->userdata('logged_in');

    $id = $this->uri->segment(3, 0);
    $this->load->model('galeria_model');
    $this->load->model('categorias_model');

    $data['categorias'] = $this->categorias_model->lista();
    $data['dados'] = $this->galeria_model->editar_cardapio($id);

    $this->load->view('admin/form-galeria-cardapio', $data);
   }
   else
   {
     //If no session, redirect to login page
     redirect('login', 'refresh');
   }
 }
function upload_galeria_cardapio()
{
  if($this->session->userdata('logged_in'))
  {
    $session_data = $this->session->userdata('logged_in');

    $config['upload_path']          = './images/galeria/';
    $config['allowed_types']        = 'gif|jpg|png';
    //$config['max_size']             = 2000;
    //$config['max_width']            = 1200;
    //$config['max_height']           = 1200;
    $config['encrypt_name']         = TRUE;

    $this->load->library('upload', $config);
    
    $this->load->model('galeria_model');    

    $this->upload->do_upload();

    $data['id'] = $this->galeria_model->id = $this->input->post('id');
    $data['idcategoria'] = $this->galeria_model->id = $this->input->post('idcategoria');
    $data['ordem'] = $this->galeria_model->ordem = $this->input->post('ordem');
    $data['titulo'] = $this->galeria_model->titulo = $this->input->post('titulo');
    $img = $this->galeria_model->img = $this->upload->data();
    $data['status'] = $this->galeria_model->status = $this->input->post('status');

    $ordens = $this->galeria_model->get_ordens($data['idcategoria']);

    if(count($ordens)<intval($data['ordem'])){
      $data['ordem'] = count($ordens)+1;
    }
    else{
      for($i=$data['ordem']; $i<=count($ordens); $i++){

        $aOrdens = array('id' => $ordens[$i-1]->id, 'ordem' => intval($ordens[$i-1]->ordem)+1);

        $this->galeria_model->atualizar_cardapio($aOrdens);
      }
    }


    if($img['file_name']!=""){
      $data['img'] = $img['file_name'];
    }

    //$data['status'] = $this->galeria_model->status = $this->input->post('status');

    /*$data['error'] = 'Banner cadastrado com sucesso!';
    $data['username'] = $session_data['username'];*/

    if(intval($data['id'])<=0){
      $this->galeria_model->insert_cardapio($data);
    }
    else{
      $this->galeria_model->atualizar_cardapio($data);
    }

    redirect('ctrl/galeria', 'refresh');

  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}
function deletar_galeria_cardapio() 
 {
    /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
    $this->load->helper('file');
    $id = $this->uri->segment(3, 0);
    $this->load->model('galeria_model');

    $data['dados'] = $this->galeria_model->editar_cardapio($id);
    //echo "<pre>"; print_r($data['dados'][0]->img);die();

    $this->load->model('galeria_model');

    //die($id);
    if($this->galeria_model->deletar_cardapio($id)) {
      $pasta = './images/galeria/';
      $arq = $data['dados'][0]->img;
      unlink($pasta.$arq);
      /* Caso sucesso ao atualizar, recarrega a página principal */
      $this->session->set_flashdata('sucesso', 'Banner deletado.');
      redirect('ctrl/galeria');
    } else {
      /* Senão exibe a mensagem de erro */
      $this->session->set_flashdata('erro', 'Não foi possível deletar o banner.');
      redirect('home');
    }
}

/* CATEGORIAS */
function list_categorias()
  {
    if($this->session->userdata('logged_in'))
    {
      $session_data = $this->session->userdata('logged_in');

      $data = array('usuario' => $session_data['username'] );

      $this->load->model('categorias_model');
      $data['dados'] = $this->categorias_model->lista();

      $this->load->view('admin/list-categorias', $data);
   }
   else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
  }
function cad_categoria()
  {
    if($this->session->userdata('logged_in'))
    {
      $this->load->model('categorias_model');
      $session_data = $this->session->userdata('logged_in');
      $data = array('usuario' => $session_data['username'] );
      $id = $this->uri->segment(3, 0);
     
      $data['dados'] = $this->categorias_model->editar($id);

      $this->load->view('admin/form-categoria', $data);
    }
    else
    {
      //If no session, redirect to login page
      redirect('login', 'refresh');
    }
  }
function deletar_categoria() 
{
  /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
  $this->load->helper('file');
  $id = $this->uri->segment(3, 0);
  $this->load->model('categorias_model');

  if($this->categorias_model->deletar($id)) {
    /* Caso sucesso ao atualizar, recarrega a página principal */
    $this->session->set_flashdata('sucesso', 'Categoria deletada.');
    redirect('ctrl/categorias');
  } else {
    /* Senão exibe a mensagem de erro */
    $this->session->set_flashdata('erro', 'Não foi possível deletar a categoria.');
    redirect('home');
  }
}
function salvar_categoria()
{
  if($this->session->userdata('logged_in'))
  {

    $session_data = $this->session->userdata('logged_in');

    $this->load->model('categorias_model');

    $data['id'] = $this->categorias_model->id = $this->input->post('id');
    $data['nome'] = $this->categorias_model->nome = $this->input->post('nome');
    //$data['slug'] = str_replace(" ", "-", $this->categorias_model->slug = utf8_decode($this->input->post('slug')));
    $data['status'] = $this->categorias_model->status = $this->input->post('status');

    
    $str = strtolower($data['nome']);
    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    $data['slug'] = str_replace(" ", "-", $str);
    //die($data['slug'].$data['nome']);

    if ($data['id']!="") {
      //die('atualizar');
      $this->categorias_model->atualizar($data);
    } else{
      //die('insert');
      $this->categorias_model->insert($data);
    }                 
    //$data['error'] = 'Notícia cadastrada com sucesso!';
    redirect('ctrl/categorias', 'refresh');
    //$this->load->view('admin/list-cidades', $data);

  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}

/* SUB-CATEGORIAS */
function list_subcategorias()
{
  if($this->session->userdata('logged_in'))
  {
    $session_data = $this->session->userdata('logged_in');
    $data = array('usuario' => $session_data['username'] );

    $this->load->model('subcategorias_model');
    $data['dados'] = $this->subcategorias_model->lista();

    $this->load->view('admin/list-subcategorias', $data);
  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}

/* GET SUB-CATEGORIAS */
function get_subcategoria()
{
    $this->load->model('subcategorias_model');

    $id = $this->subcategorias_model->id = $this->input->post('ctg');

    $data['data'] = $this->subcategorias_model->lista($id);

    echo json_encode($data['data']); die();
}

function cad_subcategoria()
{
  if($this->session->userdata('logged_in'))
  {
    $session_data = $this->session->userdata('logged_in');
    $data = array('usuario' => $session_data['username'] );
    $id = $this->uri->segment(3, 0);

    $this->load->model('categorias_model');
    $this->load->model('subcategorias_model');
     
    $data['dados'] = $this->subcategorias_model->editar($id);
    $data['categorias'] = $this->categorias_model->lista();


    $this->load->view('admin/form-subcategoria', $data);
  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}
function deletar_subcategoria()
{
  /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
  $this->load->helper('file');
  $id = $this->uri->segment(3, 0);
  $this->load->model('subcategorias_model');

  if($this->subcategorias_model->deletar($id)) {
    /* Caso sucesso ao atualizar, recarrega a página principal */
    $this->session->set_flashdata('sucesso', 'Sub-Categoria deletada.');
    redirect('ctrl/subcategorias');
  } else {
    /* Senão exibe a mensagem de erro */
    $this->session->set_flashdata('erro', 'Não foi possível deletar a sub-categoria.');
    redirect('home');
  }
}
function salvar_subcategoria()
{
  if($this->session->userdata('logged_in'))
  {

    $session_data = $this->session->userdata('logged_in');

    $this->load->model('subcategorias_model');

    $data['id'] = $this->subcategorias_model->id = $this->input->post('id');
    $data['idcategoria'] = $this->subcategorias_model->idcategoria = $this->input->post('idcategoria');
    $data['nome'] = $this->subcategorias_model->nome = $this->input->post('nome');
    $data['status'] = $this->subcategorias_model->status = $this->input->post('status');

  if ($data['id']!="") {
    //die('atualizar');
    $this->subcategorias_model->atualizar($data);
  } else{
      //die('insert');
    $this->subcategorias_model->insert($data);
  }                 
    //$data['error'] = 'Notícia cadastrada com sucesso!';
    redirect('ctrl/subcategorias', 'refresh');
    //$this->load->view('admin/list-cidades', $data);
  }
  else
    {
     //If no session, redirect to login page
     redirect('login', 'refresh');
    }
}

/* PRATOS */
function list_pratos()
{
  if($this->session->userdata('logged_in'))
  {
    $session_data = $this->session->userdata('logged_in');
    $data = array('usuario' => $session_data['username'] );

    $this->load->model('pratos_model');
    $data['dados'] = $this->pratos_model->lista();

    $this->load->view('admin/list-pratos', $data);
  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}
function cad_pratos()
{
  if($this->session->userdata('logged_in'))
  {
    $this->load->model('cidades_model');
    $session_data = $this->session->userdata('logged_in');
    $data = array('usuario' => $session_data['username'] );

    $id = $this->uri->segment(3, 0);

    $this->load->model('pratos_model');
     
    if(intval($id)>0){
      $data['dados'] = $this->pratos_model->editar($id);
    }
    else{
      $data['dados'] = array();
    }
    
    $data['subcategorias'] = $this->pratos_model->lista_categoria_subcategoria();
     
    $this->load->view('admin/form-pratos', $data);
  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}
function deletar_pratos()
{
  /* Executa a função deletar do modelo passando como parâmetro o id da pessoa */
  $this->load->helper('file');
  $id = $this->uri->segment(3, 0);
  $this->load->model('pratos_model');

  if($this->pratos_model->deletar($id)) {
    /* Caso sucesso ao atualizar, recarrega a página principal */
    $this->session->set_flashdata('sucesso', 'Prato deletado.');
    redirect('ctrl/pratos');
  } else {
    /* Senão exibe a mensagem de erro */
    $this->session->set_flashdata('erro', 'Não foi possível deletar o prato.');
    redirect('home');
  }
}
function salvar_prato()
{
  if($this->session->userdata('logged_in'))
  {

    $session_data = $this->session->userdata('logged_in');
     //$data['username'] = $session_data['username'];

    $this->load->model('pratos_model');
    //die("hhhhh");

    $data['id'] = $this->pratos_model->id = $this->input->post('id');
    $data['idsubcategoria'] = $this->pratos_model->idsubcategoria = $this->input->post('idsubcategoria');
    $data['titulo'] = $this->pratos_model->titulo = $this->input->post('titulo');
    $data['descricao'] = $this->pratos_model->descricao = $this->input->post('descricao');
    $data['status'] = $this->pratos_model->status = $this->input->post('status');

    if ($data['id']!="") {
      //die('atualizar');
      $this->pratos_model->atualizar($data);
      } else{
        //die('insert');
        $this->pratos_model->insert($data);
      }                 
    //$data['error'] = 'Notícia cadastrada com sucesso!';
    redirect('ctrl/pratos', 'refresh');
    //$this->load->view('admin/list-cidades', $data);

  }
  else
  {
    //If no session, redirect to login page
    redirect('login', 'refresh');
  }
}



  
/* LOGOUT */
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }
 
}
 
?>