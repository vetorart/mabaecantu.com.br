<?php

    /**
     *
     */
    class Principal extends CI_Controller
    {

    /**
    * Layout default utilizado pelo controlador.
    */
    public $layout = 'default';
    /**
    * Titulo default.
    */
    public $title = 'Maba & Cantu Advogados - Curitiba - PR';

    /**
    * Definindo os css default.
    */
    public $css = array('materialize/materialize.min', 'fonts', 'default');

    /**
    * Carregando os js default.
    */
    public $js = array('jquery-2.1.1.min', 'materialize.min');

    function __construct()
    {
    parent::__construct();
    $this->load->helper(array('form', 'url', 'array', 'date'));
    $this->load->library('form_validation');
    }

    // Metodoo index
    function index()
    {        

        $dados = array(
            'tags' => 'direito civil, advogados, advogados associados, advogado curitiba, advogado criminal, direito criminal, direito desportivo, advogado desportivo, advogado civil, ação civil, ação criminal, advocacia, escritório de advocacia, escritório de advocacia curitiba, curitiba advocacia, curitiba advogados, maba e cantu, maba advogados, advogados maba e cantu, cantu advogados',
            'description' => 'Escritório de advocacia em Curitiba...',
            'menu' => '1'
        );

        $dados['urlAtual'] = base_url();
        // Banner
        //$this->load->model('banners_model');
        //$this->load->model('public_model');
        // Busca inf. dos banners
        //$dados['banner'] = $this->banners_model->lista();
        // Busca produtos para a home
        //$dados['produtos'] = $this->public_model->listaProdutosHome();

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default');

       $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('home', $dados);
    }

    function empresa()
    {

        $dados = array(
            'tags' => 'advocacia, advocacia em curitiba, maba e cantu, maba e cantu advogados, advogados curitiba, advogado criminal, advogado curitiba',
            'description' => 'Profissionais especializados em direito criminal, civil e desportivo...',
            'menu' => '2',
            'banner' => 'banner-escritorio.jpg',
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú Advogados - O Escritório - Curitiba';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('empresa', $dados);
    }
    function equipe()
    {

        $dados = array(
            'tags' => 'advocacia, advocacia em curitiba, maba e cantu, maba e cantu advogados, advogados curitiba, advogado criminal, advogado curitiba',
            'description' => 'Profissionais especializados em direito criminal, civil e desportivo...',
            'menu' => '3',
            'banner' => 'banner-equipe.jpg',
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú Advogados - Equipe - Curitiba';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'equipe');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('equipe', $dados);
    }
    function areas()
    {

        $dados = array(
            'tags' => 'advocacia, advocacia em curitiba, maba e cantu, maba e cantu advogados, advogados curitiba, advogado criminal, advogado curitiba, direito civil curitiba, direito civil, direito criminal curitiba, direito criminal, direito criminal individual, direito criminal empresarial, direito desportivo, direito desportivo curitiba, direito desportivo em curitiba',
            'description' => 'Conheça um pouco mais das áreas que atuamos...',
            'menu' => '4',
            'banner' => 'banner-equipe.jpg',
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú Advogados - Equipe - Curitiba';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'equipe');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('areas-atuacao', $dados);
    }
    function direitoCivil()
    {

        $dados = array(
            'tags' => 'advocacia, advocacia em curitiba, maba e cantu, maba e cantu advogados, advogados curitiba, advogado criminal, advogado curitiba, direito civil curitiba, direito civil, direito criminal curitiba, direito criminal, direito criminal individual, direito criminal empresarial, direito desportivo, direito desportivo curitiba, direito desportivo em curitiba',
            'description' => 'Conheça um pouco mais das áreas que atuamos...',
            'menu' => '4',
            'banner' => 'banner-civil.jpg',
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú Advogados - Direito Civil - Curitiba';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'servicos');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('direito-civil', $dados);
    }
    function direitoCriminal()
    {

        $dados = array(
            'tags' => 'advocacia, advocacia em curitiba, maba e cantu, maba e cantu advogados, advogados curitiba, advogado criminal, advogado curitiba, direito criminal curitiba, direito criminal, direito criminal curitiba, direito criminal, direito criminal individual, direito criminal empresarial, direito desportivo, direito desportivo curitiba, direito desportivo em curitiba',
            'description' => 'Conheça um pouco mais das áreas que atuamos...',
            'menu' => '4',
            'banner' => 'banner-criminal.jpg',
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú Advogados - Direito Criminal - Curitiba';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'servicos');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('direito-criminal', $dados);
    }
    function direitoDesportivo()
    {

        $dados = array(
            'tags' => 'advocacia, advocacia em curitiba, maba e cantu, maba e cantu advogados, advogados curitiba, advogado desportivo, advogado curitiba, direito desportivo curitiba, direito desportivo, direito desportivo curitiba, direito desportivo, direito desportivo individual, direito desportivo empresarial, direito desportivo, direito desportivo curitiba, direito desportivo em curitiba',
            'description' => 'Conheça um pouco mais das áreas que atuamos...',
            'menu' => '4',
            'banner' => 'banner-desportivo.jpg',
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú Advogados - Direito Desportivo - Curitiba';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'servicos');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min');
        // Carregando a view.

        $this->load->view('direito-desportivo', $dados);
    }

    function busca()
    {
        $dados = array(
            'tags' => 'lavadora a vapor, jet vap, jetvap, lavadora jetvap, lavadora a vapor jet vap',
            'description' => 'Confira todos os produtos Jet Vap disponíveis.',
            'ban' => 'top-institucional.jpg',
            'link' => '1',
            'menu' => 'p',
            'send' => '0'
        );

        $busca = $this->input->post('busca', TRUE);

        $this->load->model('produtos_model');
        $dados['busca'] = $this->produtos_model->busca_produto($busca);

        $dados['termo'] = $busca;

        $this->layout = 'default-busca';

        $this->title = 'BUSCA - Jet Vap - Lavadoras a Vapor';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'produtos');

        $this->js = array('jquery-2.1.1.min', 'materialize.min');
        // Carregando a view.

        $this->load->view('busca', $dados);
    }
    

    function contato()
    {
        /* API GOOGLE MAPS */
        $this->load->library('googlemaps'); 

        $config['center'] = '-25.515854, -49.327457';
        $config['zoom'] = '15';
        $config['map_height'] = '450px';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = '-25.515854, -49.327457';
        $marker['infowindow_content'] = "Maba & Cantú - 41 3347.1742";
        $this->googlemaps->add_marker($marker);

        $dados = array(
            'tags' => 'advogados, advocacia, advogado curitiba, advogado maba curitiba, advogado cantu curitiba, advocado em curitiba, escritório de advocacia curitiba',
            'description' => 'Fale conosco e conheça um pouco mais do nosso trabalho...',
            'banner' => 'banner-contato.jpg',
            'menu' => '5'
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);
        
        $dados['map'] = $this->googlemaps->create_map();

        $this->layout = 'default-interna';

        $this->title = 'Maba & Cantú - Fale Conosco - Curitiba - PR';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'servicos', 'contato');

        $this->js = array('jquery-2.1.1.min', 'materialize.min', 'slick.min', 'youtubepopup.jquery');
        // Carregando a view.

        $this->load->view('contato', $dados);
    }
    // Todas as Notícias
    function noticias()
    {
        $dados = array(
            'tags' => 'scanners, scanners fujitsu, fujitsu scanners, scanner, aluguel de scanner, manutenção de scanners, manutenção de scanners fujitsu, concerto de scanner',
            'description' => 'Scanners Fujitsu para locação, venda e manutenção...',
            'menu' => '6',
            'imgTop' => 'topScan.jpg',
            'h1' => 'Notícias',
            'h2' => 'Dicas e Novidades'
        );

        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0);

        $this->load->model('noticias_model');
        $dados['not'] = $this->noticias_model->lista_noticias();

        $this->layout = 'default-interna';

        $this->title = 'Notícias - Núcleo Básico';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'noticias');

        $this->js = array('jquery-2.1.1.min', 'materialize.min');
        // Carregando a view.

        $this->load->view('noticias', $dados);
    }
    // Página do produto individual
    function noticia()
    {
        $this->load->model('noticias_model');

        $slug = $this->uri->segment(2, 0);
        $dados['noticia'] = $this->noticias_model->noticia($slug);
        $news = $this->noticias_model->noticia($slug);

        // SEO
        foreach ($news as $news) {
            $dados['tags'] = $news->tags;
            $dados['description'] = $news->description;
            $dados['texto'] = $news->texto;
            $dados['img'] = $news->img;
            $dados['tags'] = $news->tags;
            $dados['nome'] = $news->titulo;
            $dados['h1'] = $news->titulo;
        }
        $dados['menu'] = '6';
        $dados['send'] = '0';
        $dados['h2'] = '';
        $dados['imgTop'] = 'topScan.jpg';
        $dados['urlAtual'] = base_url().$this->uri->segment(1, 0).'/'.$this->uri->segment(2, 0);

        $this->title = $dados['nome'];

        $this->layout = 'default-interna';

        $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'noticia');

        $this->js = array('jquery-2.1.1.min', 'materialize.min');
        // Carregando a view.

        $this->load->view('noticia', $dados);
    }
    // Envia e-mail.
    function sendMail(){
        $urlAtual = $this->input->post('urlAtual', TRUE);

        /*
        $config['smtp_host'] = 'email-ssl.com.br';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = 'dev@vetorart.com.br';
        $config['smtp_pass'] = 'vapor@2016';
        $config['protocol']  = 'smtp';
        $config['validate']  = TRUE;
        */

        $this->load->library('email');

        $this->email->set_mailtype("html");

        $nome = $this->input->post('nome', TRUE);
        $email = $this->input->post('email', TRUE);
        $cidade = $this->input->post('cidade', TRUE);
        $telefone = $this->input->post('telefone', TRUE);

        $this->email->from('contato@mabaecantu.com.br');
        $this->email->to('contato@mabaecantu.com.br'); 
        $this->email->cc('mariana.cantu@mabaecantu.com.br'); 
        $this->email->bcc('djonathan.maba@mabaecantu.com.br'); 

        $msg = $this->input->post('mensagem', TRUE);

        // Monta a mensagem HTML que será enviada.
        $mensagem = "CONTATO DO SITE<br><br>Nome: ".$nome."&nbsp;| ".$telefone."<br><br>E-mail: ".$email."<br><br>Cidade: ".$cidade."<br><br>Mensagem:<br>".$msg;

        $this->email->subject('CONTATO DO SITE');
        $this->email->message($mensagem);

        if ($this->email->send()) {
            $dados = array(
                'tags' => 'advocacia', 'sucesso' => $this->session->set_flashdata('sucesso','E-mail enviado com sucesso! Aguarde que logo lhe responderemos.')
            );

            $this->layout = 'default-interna';

            $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'send');

            $this->js = array('jquery-2.1.1.min', 'materialize.min');
                    // Carregando a view.
            redirect($urlAtual, 'refresh');

        } else {
            $dados = array(
                'tags' => 'advogados curitiba',
                'description' => '',
                'msg' => 'Erro ao enviar a mensagem, por favor, entre em contato através do telefone.'
            );
        };
    }
    function retornaLigacao()
    {
        /*
        $config['smtp_host'] = 'email-ssl.com.br';
        $config['smtp_port'] = 465;
        $config['smtp_user'] = 'dev@vetorart.com.br';
        $config['smtp_pass'] = 'vapor@2016';
        $config['protocol']  = 'smtp';
        $config['validate']  = TRUE;
        */
        $this->load->library('email');

        $this->email->set_mailtype("html");

        $urlAtual = $this->input->post('urlAtual', TRUE);
        $produto = $this->input->post('produto', TRUE);

        $nome = $this->input->post('nome', TRUE);
        $telefone = $this->input->post('telefone', TRUE);

        $this->email->from('dev@vetorart.com.br');
        $this->email->to('dev@vetorart.com.br');
        //$this->email->cc('outro@outro-site.com'); 
        //$this->email->bcc('outro@hotmail.com'); 

        
        // Se vier de uma página de produto
        $mensagem = "PEDIDO DE CONTATO - PRODUTO<br><br>Produto: ".$produto."<br><br>Nome: ".$nome."&nbsp;| ".$telefone."<br><br>Produto:".$produto."";

        $this->email->subject('PEDIDO DE CONTATO - SITE');
        $this->email->message($mensagem);

        if ($this->email->send()) {
            $dados = array(
                'tags' => '', 'sucesso' => $this->session->set_flashdata('sucesso','E-mail enviado com sucesso! Aguarde que logo lhe responderemos.')
            );

            $this->layout = 'default-interna';

            $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'send');

            $this->js = array('jquery-2.1.1.min', 'materialize.min');
                    // Carregando a view.
            redirect($urlAtual, 'refresh');

        } else {
            $dados = array(
                'tags' => '', 'sucesso' => $this->session->set_flashdata('sucesso','ERRO! Por favor, entre em contato por telefone.')
            );

            $this->layout = 'default-interna';

            $this->css = array('materialize/materialize.min','slick/slick', 'slick/slick-theme', 'fonts', 'default', 'send');

            $this->js = array('jquery-2.1.1.min', 'materialize.min');
                    // Carregando a view.
            redirect($urlAtual, 'refresh');
        };
    }

    

    
}