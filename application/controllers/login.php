<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Login extends CI_Controller {

	public $layout = 'default-login';

	public $css = array('materialize/materialize.min', 'login');

    public $js = array('jquery-2.1.1.min', 'materialize.min');
 
 function __construct()
 {
   parent::__construct();
   $this->load->helper(array('form', 'url', 'array', 'date'));
   $this->load->library('form_validation', 'session');
 }
 
 function index()
 {
   $this->load->helper(array('form'));
   $this->load->view('login_view');
 }
 function recovery_pass()
 {
 	

 	$this->load->view('recovery-pass');
 }
 function sendpass()
 {

 	$this->load->view('send-pass');
 }
 
}
 
?>