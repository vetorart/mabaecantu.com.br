<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{title_for_layout}</title>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

{css_for_layout}

{js_for_layout}

<script>
$(document).ready(function(){
	$(".button-collapse").sideNav();
	$('select').material_select();
})	
</script>

</head>

<body>

<header>
	<div class="row centraliza">
		<a href="<?php echo base_url()?>home" class="brand-logo">
			<img src="<?php echo base_url();?>images/logo-vmanager.png" alt="" class="responsive-img" />
		</a>
		<div class="hide-on-med-and-down right saudacao">
			<a href="<?php echo base_url()?>ctrl/change-password">TROCAR SENHA</a>
		</div>
		<div class="hide-on-med-and-down right saudacao btnh">
			<a href="<?php echo base_url()?>" target="_blank" title="Visitar site"><span class="fa fa-home"></span></a>
		</div>
		<div class="hide-on-large-only right">
		<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons mmob">menu</i></a>
		<ul class="side-nav menutop" id="mobile-demo">
	        <li><a href="<?php echo base_url()?>ctrl/banners">Banners</a></li>
	        <li><a href="<?php echo base_url()?>ctrl/produtos">Produtos</a></li>
	        <!-- <li><a href="<?php echo base_url()?>ctrl/videos">Vídeos</a></li>
	        <li><a href="<?php echo base_url()?>ctrl/noticias">Notícias</a></li>
	         -->
	        <li><a href="<?php echo base_url()?>home/logout">Sair</a></li>
	    </ul>
		</div>
	</div>
</header>

<div class="menu centraliza hide-on-med-and-down">
	<nav>
	    <div class="nav-wrapper">
	      
	      <ul class="left hide-on-med-and-down menutop">
	        <li><a href="<?php echo base_url()?>ctrl/banners">Banners</a></li>
	        <li><a href="<?php echo base_url()?>ctrl/produtos">Produtos</a></li>
	        <li><a href="<?php echo base_url()?>ctrl/noticias">Notícias</a></li>
	        <!-- 
	        <li><a href="<?php echo base_url()?>ctrl/videos">Vídeos</a></li>
	         -->
	        <li><a href="<?php echo base_url()?>home/logout">Sair</a></li>
	      </ul>
	    </div>
	</nav>
</div>


<div id="conteudo">
    {content_for_layout}
</div>

<footer>
	
</footer>

</body>
</html>