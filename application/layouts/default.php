<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{title_for_layout}</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>/images/favicon.png" />
<meta name="robots" content="index, follow">
<meta name="keywords" content="<?php echo $tags; ?>" />
<meta name="description" content="<?php echo $description; ?>" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

{css_for_layout}

{js_for_layout}

<?php $sucesso = $this->session->flashdata('sucesso'); ?>

<script>
$(document).ready(function(){
    $window = $(window);
    //Captura cada elemento section com o data-type "background"
    $('#bannerHome').each(function(){
        var $scroll = $(this);   
            //Captura o evento scroll do navegador e modifica o backgroundPosition de acordo com seu deslocamento.            
            $(window).scroll(function() {
                var yPos = -($window.scrollTop() / $scroll.data('speed')); 
                var coords = '50% '+ yPos + 'px';
                $scroll.css({ backgroundPosition: coords });    
            });
   });  
});

$(document).ready(function(){
  var sucesso = "<?php echo $sucesso; ?>";
  Materialize.toast(sucesso, 6000, 'rounded');
  $('.carousel.carousel-slider').carousel({fullWidth: true});
  $(".button-collapse").sideNav();
  $(".dropdown-button").dropdown({hover: true});
  $('select').material_select();
  $('.scrollspy').scrollSpy();
  $(".owl-carousel").owlCarousel({items : 1,autoPlay : true,stopOnHover : true,});

  $(window).load(function() {
      $('.flexslider').flexslider();
  });

  /*  */
  $window = $(window);
    //Captura cada elemento section com o data-type "background"
    $('div[data-type="background"]').each(function(){
        var $scroll = $(this);   
            //Captura o evento scroll do navegador e modifica o backgroundPosition de acordo com seu deslocamento.            
            $(window).scroll(function() {
                var yPos = -($window.scrollTop() / $scroll.data('speed')); 
                var coords = '50% '+ yPos + 'px';
                $scroll.css({ backgroundPosition: coords });    
            });
   });
})  
/* jump menu footer */
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}


  /* Máscaras ER */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}
function id( el ){
  return document.getElementById( el );
}
window.onload = function(){
  id('telefone').onkeypress = function(){
    mascara( this, mtel );
  }
}


</script>

</head>
<body>

<nav class="menu">
    <div class="nav-wrapper">
      <div class="centraliza">
        <a href="<?php echo base_url(); ?>" class="brand-logo"><img src="<?php echo base_url();?>images/logo.jpg" alt="Projetart Divisórias" class="responsive-img" /></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li>
            <a <?php if ($menu == 1) { echo "class='mactive'";} ?> href="<?php echo base_url(); ?>">Home</a>
          </li>
          <li>
            <a <?php if ($menu == 2) { echo "class='mactive'";} ?> href="<?php echo base_url(); ?>o-escritorio">O Escritório</a>
          </li>
          <li>
            <a <?php if ($menu == 3) { echo "class='mactive'";} ?> href="<?php echo base_url(); ?>equipe">Equipe</a>
          </li>
          <li>
            <a class="dropdown-button" <?php if ($menu == 4) { echo "class='dropdown-button mactive'";} ?> href="#!" data-activates="dropdown">Áreas de atuação</a>
          </li>
          <li>
            <a <?php if ($menu == 4) { echo "class='mactive'";} ?> href="<?php echo base_url(); ?>contato">Contato</a>
          </li>
        </ul>
        <ul class="side-nav" id="mobile-demo">
          <li><a href="<?php echo base_url(); ?>">Home</a></li>
          <li><a href="<?php echo base_url(); ?>o-escritorio">O Escritório</a></li>
          <li><a href="<?php echo base_url(); ?>equipe">Equipe</a></li>
          <li><a href="<?php echo base_url(); ?>areas-de-atuacao">Áreas de Atuação</a></li>
          <li><a href="<?php echo base_url(); ?>contato">Contato</a></li>
        </ul>
      </div>
    </div>
</nav>
<ul id="dropdown" class="dropdown-content collection" style="min-width:230px !important;">
    <li>
      <a href="<?php echo base_url();?>direito-civil">Direito Civil</a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="<?php echo base_url();?>direito-criminal">Direito Criminal</a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="<?php echo base_url();?>direito-desportivo">Direito Desportivo</a>
    </li>
  </ul>

<div id="bannerHome" data-speed="5" data-type="background">
  <div class="row centraliza">
    <div class="col s12 m5">&nbsp;</div>
    <div class="col s12 m7">
      <div class="txtbanner">
        O escritório Maba & Cantú Advogados tem como principal objetivo o zelo pela liberdade e direitos de seus clientes, por meio da prestação de um serviço comprometido, responsável, ético e eficiente, a fim de alcançar-se o máximo sucesso nas causas que lhe são outorgadas.
      </div>
      <a href="<?php echo base_url();?>o-escritorio" class="saib right">SAIBA MAIS</a>        
    </div>
  </div>
</div>
<div class="clear"></div>

<?php /*
<div class="slider bannerHome">
  <div class="flexslider" style="display:block;">
    <ul class="slides">
      <?php foreach ($banner as $ban):?>
        <li>
          <a href="<?php echo $ban->url; ?>">
            <img class="hide-on-small-only" src="<?php echo base_url()."images/banners/".$ban->img; ?>" alt="<?php echo $ban->nome; ?>"/>
            <img class="hide-on-med-and-up" src="<?php echo base_url()."images/banners/".$ban->imgmobile; ?>" alt="<?php echo $ban->nome; ?>"/>
          </a>
        </li>
      <?php endforeach ?>
    </ul>
  </div>  
</div>
<div class="clear"></div>
*/ ?>
<section id="conteudo">
    {content_for_layout} 
</section>


<footer>
  <div class="rodapeBlack">
    <div class="centraliza">
      <div class="row">
        <div class="col s12 m5" align="center">
          <img src="<?php echo base_url();?>images/logo-footer.jpg" alt="Maba e Cantú" class="responsive-img" />
        </div>
        <div class="col s12 m7">
          <div class="row no-padding no-margin">
            <div class="col s12 m6 no-padding-x infooter">
              <span>MENU</span>
              <ul>
                <li>
                  <a href="<?php echo base_url();?>">Home</a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>o-escritorio">O Escritório</a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>areas-de-atuacao">Áres de atuação</a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>equipe">Equipe</a>
                </li>
                <li>
                  <a href="<?php echo base_url();?>contato">Contato</a>
                </li>
              </ul>
            </div>
            <div class="col s12 m6 no-padding-x infooter">
              <span>REDES SOCIAIS</span>
              <a href="https://www.facebook.com/mabaecantuadvogados/" target="_blank" class="left mt20 mb20">
                <img src="<?php echo base_url();?>images/iconfacebook.jpg" alt="Facebook" class="responsive-img" />
              </a>
              <a href="www.instagram.com/mabaecantuadvogados" target="_blank" class="left mt20 mb20">
                <img src="<?php echo base_url();?>images/iconinsta.jpg" alt="Instagram" class="responsive-img" />
              </a>
              <a href="www.twitter.com/mabaecantuadvogados" target="_blank" class="left mt20 mb20">
                <img src="<?php echo base_url();?>images/icontwitter.jpg" alt="Twitter" class="responsive-img" />
              </a>
              <div class="clear"></div>
              <img src="<?php echo base_url();?>images/icontel.jpg" alt="Telefones" class="img-responsive left hide-on-small-only" />
              <div class="left dtef">(41) 3347-1742</div>
              <div class="clear"></div>
              <img src="<?php echo base_url();?>images/iconmail.jpg" alt="E-mail" class="img-responsive left hide-on-small-only mt5" />
              <div class="left dtef2">contato@mabaecantu.com.br</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="direitos" align="center">
    <strong>2017 - todos os direitos reservados - maba & cantú Advogados</strong> <br />
    <span>Site desenvolvido por: <a href="http://vetorart.com.br" target="_blank">VetorArt Design</a></span>
  </div>

</footer>



</body>
</html>