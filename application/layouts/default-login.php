<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{title_for_layout}</title>
<link rel="shortcut icon" href="<?php echo base_url(); ?>/images/favicon.png" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

{css_for_layout}

{js_for_layout}

<script>
$(document).ready(function(){
	$(".button-collapse").sideNav();
})	
</script>

</head>

<body>




<div id="conteudo">
    {content_for_layout}
</div>

<footer>
	
</footer>

</body>
</html>