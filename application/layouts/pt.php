<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<title>{title_for_layout}</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

{css_for_layout}

{js_for_layout}

</head>

<body>

    <div id="geral">

        <div id="topo">
            
        </div>

        <div id="meio">
	

			<div id="conteudo">
				{content_for_layout}
			</div>
		
		</div>

        

    </div>



</body>
</html>