<?php
Class User extends CI_Model
{
 function login($username, $password)
 {
   $this -> db -> select('id, nome, email, usuario, senha');
   $this -> db -> from('usuarios');
   $this -> db -> where('usuario', $username);
   $this -> db -> where('senha', MD5($password));
   $this -> db -> limit(1);
 
   $query = $this -> db -> get();
 
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }
 function recovery($email)
 {
   $this -> db -> select('id, nome, email, usuario, senha');
   $this -> db -> from('usuarios');
   $this -> db -> where('email', $email);
   $this -> db -> limit(1);
 
   $query = $this -> db -> get();
 
   if($query -> num_rows() == 1)
   {
     //die('Validou');
     return $query->result();
   }
   else
   {
      //die('Não validou');
     return false;
   }
 }
function trocar_senha($data) {
  $this->db->where('id', $data['id']);
  $this->db->set($data);
  return $this->db->update('usuarios');
}
function editar_senha($idUser) {
  $this->db->where('id', $idUser);
  $query = $this->db->get('usuarios');
  return $query->result();
}

function atualizar_nova_senha($data) {
  $this->db->where('id', $data['id']);
  $this->db->set($data);
  return $this->db->update('usuarios');
}

}
?>