<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Produtos_model extends CI_Model {

    protected $_table = 'produtos';
    protected $_pk = 'id';

    function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $dados = array('nome' => $data['nome'], 'url' => $data['url']
            , 'img' => $data['img'], 'status' => $data['status']);
        $this->db->insert($this->_table, $dados);
    }
    function inserirProduto($data) {
        $dados = array('ordem' => $data['ordem'], 'nome' => $data['nome'], 'idcategoria' => $data['idcategoria'], 'slug' => $data['slug'], 'video' => $data['video'], 'img' => $data['img'], 'tags' => $data['tags'], 'bdesc' => $data['bdesc'], 'description' => $data['description'], 'descricao' => $data['descricao'], 'especificacao' => $data['especificacao'], 'garantia' => $data['garantia'], 'status' => $data['status']);
        $this->db->insert($this->_table, $dados);
    }

    function lista_produtos() {
        $this->db->order_by('ordem', 'ASC');
        $this->db->where('status', 'Ativo');
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function listaProdutos($idcat) {
        $this->db->order_by('ordem', 'ASC');
        $this->db->where('idcategoria', $idcat);
        $this->db->where('status', 'Ativo');
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function orders() {
        $this->db->order_by('ordem', 'ASC');
        //$this->db->where('status', 'Ativo');
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function busca_produto($busca) {
        $this->db->order_by('ordem', 'DESC');
        $this->db->like('nome', $busca);
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function listaADM() {
        $this->db->order_by('ordem', 'ASC');
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function produto($slug) {
        $this->db->where('slug', $slug);
        $query = $this->db->get('produtos');
        return $query->result();
    }

    function editar($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update($this->_table);
    }
    function prodAtual($ordemAtual) {
        $this->db->where('ordem', $ordemAtual);
        $query = $this->db->get('produtos');
        return $query->result();
    }
    function atualizaOrdem($v1, $v2) {
        $this->db->order_by('ordem', 'ASC');
        //$query = $this->db->get($this->_table);
        $query = $this->db->select('id, ordem, nome')
                          ->from('produtos')
                          //->where('p.ordem > 1000')
                          ->where($v1)
                          ->where($v2)
                          ->get();
        return $query->result();        
    }
    function OrgDel($v1, $v2) {
        $this->db->order_by('ordem', 'ASC');
        //$query = $this->db->get($this->_table);
        $query = $this->db->select('id, ordem, nome')
                          ->from('produtos')
                          //->where('p.ordem > 1000')
                          ->where($v1)
                          ->where($v2)
                          ->get();
        return $query->result();        
    }
    function reordenar($dados) {
        $this->db->where($this->_pk, $dados['id']);
        $this->db->set('id', $dados['id']);
        $this->db->set('ordem', $dados['ordem']);
        return $this->db->update($this->_table);
    }
    function reordenar_inicial($inicial) {
        $this->db->where($this->_pk, $inicial['id']);
        $this->db->set($inicial);
        return $this->db->update('produtos', array('ordem' => $inicial['ordem']));
    }

    /*
    function reordenar($infs) {
        $this->db->where($this->_pk, $infs['idp']);
        $this->db->set($infs['idp']);
        return $this->db->update('produtos', array('ordem' => $infs['idp']->ordem));
    }
    */

    function deletar($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete($this->_table);
    }
}