<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banners_model extends CI_Model {

    protected $_table = 'banners';
    protected $_pk = 'id';

    function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $dados = array('nome' => $data['nome'], 'url' => $data['url']
            , 'imgmobile' => $data['imgmobile'], 'img' => $data['img'], 'status' => $data['status']);
        $this->db->insert($this->_table, $dados);
    }

    function lista() {
        $this->db->order_by('id', 'DESC');
        $this->db->where('status', 'Ativo');
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function lista_banners() {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function editar($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update($this->_table);
    }

    function deletar($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete($this->_table);
    }
}