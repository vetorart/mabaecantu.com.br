<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subcategorias_model extends CI_Model {

    protected $_table = 'subcategorias';
    protected $_pk = 'id';

    function __construct() {
        parent::__construct();
    }

    public function insert($data) {
        $dados = array('idcategoria' => $data['idcategoria'], 'nome' => $data['nome'], 'status' => $data['status']);
        $this->db->insert($this->_table, $dados);
    }

    function lista($id=null) {
        $this->db->select('subcategorias.id, categorias.nome as categoria, subcategorias.nome, subcategorias.status');
        $this->db->from('subcategorias');
        $this->db->join('categorias', 'subcategorias.idcategoria = categorias.id');

        if($id!=null){
            $this->db->where('idcategoria', $id);
        }
        

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function lista_subcategorias() {$this->db->order_by('nome', 'ASC');
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function editar($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update($this->_table);
    }

    function deletar($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete($this->_table);
    }
}