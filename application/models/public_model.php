<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Public_model extends CI_Model {

    protected $_table = 'newsletter';
    protected $_pk = 'id';
    protected $status = 'status';
    protected $slug = 'slug';

    function __construct() {
        parent::__construct();
    }

    public function insert_news($data) {
        $dados = array('email' => $data['email']);
        $this->db->insert($this->_table, $dados);
    }
    function lista_noticias() {
        $this->db->where($this->status, 'Ativo');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('noticias');
        return $query->result();
    }
    function listaProdutosHome() {
        $this->db->where($this->status, 'Ativo');
        $this->db->limit(3);
        $this->db->order_by('ordem', 'ASC');
        $query = $this->db->get('produtos');
        return $query->result();
    }

    function novidade($slug) {
        $this->db->where('slug', $slug);
        $query = $this->db->get('noticias');
        return $query->result();
    }
    function cidade($slug_cidade) {
        $this->db->where($this->slug, $slug_cidade);
        $query = $this->db->get($this->_table);
        return $query->result();
    } 
    function menu_categorias($slug_cidade) {
        $this->db->select('DISTINCT(idcategoria),categorias.nome as categoria, categorias.slug');
        $this->db->from('cardapios');
        $this->db->join('cidades', 'cidades.id = cardapios.idcidade');
        $this->db->join('categorias', 'categorias.id = cardapios.idcategoria');
        $this->db->where('cidades.slug', $slug_cidade);
        $this->db->order_by('categoria');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
    function galeria_cardapio($slug_categoria) {
        $this->db->select('titulo, img');
        $this->db->from('galeria_cardapio');
        $this->db->join('categorias', 'categorias.id = galeria_cardapio.idcategoria');
        $this->db->where('categorias.slug', $slug_categoria);
        $this->db->order_by('ordem');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }
    function lista_galeria_cidade($slug_cidade) {
        $this->db->select('titulo, img');
        $this->db->from('galeria_cidade');
        $this->db->join('cidades', 'cidades.id = galeria_cidade.idcidade');
        $this->db->where('cidades.slug', $slug_cidade);
        $this->db->where('galeria_cidade.status', 'Ativo');
        $this->db->order_by('galeria_cidade.id', 'DESC');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function menu_subcategorias($slug_cidade, $slug_categoria) {
        $this->db->select('DISTINCT(idsubcategoria),subcategorias.nome as subcategoria');
        $this->db->from('cardapios');
        $this->db->join('cidades', 'cidades.id = cardapios.idcidade');
        $this->db->join('categorias', 'categorias.id = cardapios.idcategoria');
        $this->db->join('subcategorias', 'subcategorias.id = cardapios.idsubcategoria');
        $this->db->where('cidades.slug', $slug_cidade);
        $this->db->where('categorias.slug', $slug_categoria);
        $this->db->order_by('subcategoria');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function menu_pratos($slug_cidade, $idsubcategoria) {
        $this->db->select('titulo,descricao,valor');
        $this->db->from('cardapios');
        $this->db->join('cidades', 'cidades.id = cardapios.idcidade');
        $this->db->join('subcategorias', 'subcategorias.id = cardapios.idsubcategoria');
        $this->db->join('pratos', 'pratos.id = cardapios.idprato');
        $this->db->where('cidades.slug', $slug_cidade);
        $this->db->where('cardapios.idsubcategoria', $idsubcategoria);
        $this->db->order_by('titulo');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function lista_precos($slug_cidade) {
        $this->db->where($this->slug, $slug_cidade);
        $this->db->select('precos.nome as preconome, precos.valor, cidades.nome');
        $this->db->from('precos');
        $this->db->join('cidades', 'cidades.id = precos.idcidades');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }   
    function lista_subcategorias() {
        $this->db->where('subcategorias', 'Ativo');
        $this->db->order_by($this->ordem, 'ASC');
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function editar($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update($this->_table);
    }

    function deletar($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete($this->_table);
     }
}