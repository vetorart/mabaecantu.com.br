<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Galeria_model extends CI_Model {

    protected $_table = 'galeria';
    protected $_pk = 'id';

    function __construct() {
        parent::__construct();
    }

    /* INSERÇÃO ADMIN */
    public function insert_cardapio($data) {
        $dados = array('idcategoria' => $data['idcategoria'], 'ordem' => $data['ordem'], 'titulo' => $data['titulo'], 'img' => $data['img'], 'status' => $data['status']);
        $this->db->insert('galeria_cardapio', $dados);
    }
    function insert_cidade($data) {
        $dados = array('idcidade' => $data['idcidade'], 'titulo' => $data['titulo'], 'img' => $data['img'], 'status' => $data['status']);
        $this->db->insert('galeria_cidade', $dados);
    }

    /* LISTAGEM ADMIN */
    function lista_cardapio() {
        $this->db->select('galeria_cardapio.id, categorias.nome as categoria, galeria_cardapio.idcategoria, galeria_cardapio.ordem, galeria_cardapio.titulo, galeria_cardapio.img, galeria_cardapio.status');
        $this->db->from('galeria_cardapio');
        $this->db->join('categorias', 'galeria_cardapio.idcategoria = categorias.id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function lista_cidade() {
        $this->db->select('galeria_cidade.id, cidades.nome as cidade, galeria_cidade.idcidade, galeria_cidade.titulo, galeria_cidade.img, galeria_cidade.status');
        $this->db->from('galeria_cidade');
        $this->db->join('cidades', 'galeria_cidade.idcidade = cidades.id');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function get_ordens($idcategoria) {
        $this->db->select('id, ordem');
        $this->db->from('galeria_cardapio');
        $this->db->where('idcategoria', $idcategoria);
        $this->db->where('status', 'Ativo');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    /* EDIÇÃO ADMIN */
    function editar_cardapio($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get('galeria_cardapio');
        return $query->result();
    }
    function editar_cidade($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get('galeria_cidade');
        return $query->result();
    }
    function atualizar_cardapio($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update('galeria_cardapio');
    }
    function atualizar_cidade($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update('galeria_cidade');
    }

    /* DELETAR ADMIN */
    function deletar_cardapio($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete('galeria_cardapio');
    }
    function deletar_cidade($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete('galeria_cidade');
    }
}