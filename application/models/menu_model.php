<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model {

    protected $_table = 'cidades';
    protected $_pk = 'id';
    protected $id = '10';
    protected $ordem = 'nome';
    protected $status = 'status';
    protected $slug = 'slug';

    function __construct() {
        parent::__construct();
    }

    function lista_cidades() {
        $this->db->where($this->status, 'Ativo');
        $this->db->order_by($this->ordem, 'ASC');
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function lista_delivery() {
        $this->db->where('delivery !=', '');
        $this->db->order_by('nome');
        $query = $this->db->get($this->_table);
        return $query->result();
    }
    function cidade($slug) {
        $this->db->where($this->slug, $slug);
        $query = $this->db->get($this->_table);
        return $query->result();
    }    
    function lista_precos($slug) {
        $this->db->where($this->slug, $slug);
        $this->db->select('precos.nome as preconome, precos.valor as preco, cidades.id');
        $this->db->from('precos');
        $this->db->join('cidades', 'cidades.slug = precos.slugcidades');

        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

    function editar($id) {
        $this->db->where($this->_pk, $id);
        $query = $this->db->get($this->_table);
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where($this->_pk, $data['id']);
        $this->db->set($data);
        return $this->db->update($this->_table);
    }

    function deletar($id) {
         $this->db->where($this->_pk, $id);
         return $this->db->delete($this->_table);
     }
}