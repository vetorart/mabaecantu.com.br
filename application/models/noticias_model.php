<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends CI_Model {


    function __construct() {
        parent::__construct();
    }

    

    public function insert($data) {
        $dados = array('datacad' => $data['datacad'], 'titulo' => $data['titulo'], 'description' => $data['description'], 'bdesc' => $data['bdesc'], 'slug' => $data['slug'], 'img' => $data['img'], 'texto' => $data['texto'], 'status' => $data['status'], 'tags' => $data['tags']);
		$this->db->insert('noticias', $dados);
	}

	function lista_noticias() {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('noticias');
        return $query->result();
    }
    function noticia($slug) {
        $this->db->where('slug', $slug);
        $query = $this->db->get('noticias');
        return $query->result();
    }

    function listnews_three() {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('noticias', 3);
		//$query = $this->db->limit(3);
		return $query->result();
	}

    function retornaLista($maximo, $inicio)
    {
     $query = $this->db->get('noticias', $maximo, $inicio);
     return $query->result();
    }

    function editar($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('noticias');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id', $data['id']);
        $this->db->set($data);
        return $this->db->update('noticias');
    }

    function deletar($id) {
         $this->db->where('id', $id);
         return $this->db->delete('noticias');
     }
}