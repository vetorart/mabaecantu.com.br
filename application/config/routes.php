<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "principal";
$route['404_override'] = '';

$route['o-escritorio'] = "principal/empresa";
$route['equipe'] = "principal/equipe";
$route['areas-de-atuacao'] = "principal/areas";
$route['direito-civil'] = "principal/direitoCivil";
$route['direito-criminal'] = "principal/direitoCriminal";
$route['direito-desportivo'] = "principal/direitoDesportivo";


$route['contato'] = "principal/contato";
$route['call-return'] = "principal/retornaLigacao";
$route['enviar-email'] = "principal/sendMail";
//$route['contato-produto'] = "principal/sendMailP";

$route['login'] = "login/index";
$route['recuperar-senha'] = "login/recovery_pass";
$route['send-pass'] = "login/sendpass";
$route['ctrl/change-password'] = "home/trocar_senha";


$route['noticias'] = 'principal/noticias';
$route['noticia/(:any)'] = 'principal/noticia/$1';


/*$route['noticia/(:any)'] = 'principal/noticia/$1';
$route['noticia'] = 'principal';

$route['noticia/(:any)'] = "principal/noticia/$1"; */


//Urls da área administrativa
/* LOGIN */
$route['ctrl'] = "login";
/* NOTÍCIAS */
$route['ctrl/noticias'] = "noticias/list_noticias";
$route['ctrl/cad-noticia'] = "noticias/cad_noticia";
$route['ctrl/cad-noticia/(:any)'] = "noticias/cad_noticia/$1";
/* BANNERS */
$route['ctrl/banners'] = "banners/list_banners";
$route['ctrl/cad-banner'] = "banners/cad_banner";
$route['ctrl/cad-banner/(:any)'] = "banners/cad_banner/$1";
/* VÍDEOS */
$route['ctrl/videos'] = "videos/list_videos";
$route['ctrl/cad-video'] = "videos/cad_video";
$route['ctrl/cad-video/(:any)'] = "videos/cad_video/$1";
$route['ctrl/videos/deletar/(:any)'] = "videos/deletar_video/$1";
/* QUEM SOMOS */
$route['ctrl/quemsomos'] = "quemsomos/list_quemsomos";
$route['ctrl/cad-quemsomos'] = "quemsomos/cad_quemsomos";
$route['ctrl/cad-quemsomos/(:any)'] = "quemsomos/cad_quemsomos/$1";
/* PRODUTOS */
$route['ctrl/produtos'] = "produtos/list_produtos";
$route['ctrl/cad-produto'] = "produtos/cad_produto";
$route['ctrl/cad-produto/(:any)'] = "produtos/cad_produto/$1";
$route['ctrl/produtos/ordenar/(:any)'] = "produtos/ordenar/$1";





/* GALERIA - CIDADE */
$route['ctrl/galeria'] = "home/list_galeria";
$route['ctrl/cad-galeria-cidade'] = "home/cad_galeria_cidade";
$route['ctrl/cad-galeria-cidade/(:any)'] = "home/cad_galeria_cidade/$1";
/* GALERIA - CARDAPIO */
$route['ctrl/cad-galeria-cardapio'] = "home/cad_galeria_cardapio";
$route['ctrl/cad-galeria-cardapio/(:any)'] = "home/cad_galeria_cardapio/$1";
/* CIDADES */
$route['ctrl/cidades'] = "home/list_cidades";
$route['ctrl/cad-cidade'] = "home/cad_cidade";
$route['ctrl/cad-cidade/(:any)'] = "home/cad_cidade/$1";
/* RODÍZIO */
$route['ctrl/rodizio'] = "home/list_rodizio";
$route['ctrl/cad-rodizio'] = "home/cad_rodizio";
$route['ctrl/cad-rodizio/(:any)'] = "home/cad_rodizio/$1";
/* CARDÁPIO */
$route['ctrl/cardapio'] = "home/list_cardapio";
$route['ctrl/cad-cardapio'] = "home/cad_cardapio";
$route['ctrl/cad-cardapio/(:any)'] = "home/cad_cardapio/$1";
/* CATEGORIAS */
$route['ctrl/categorias'] = "home/list_categorias";
$route['ctrl/cad-categoria'] = "home/cad_categoria";
$route['ctrl/cad-categoria/(:any)'] = "home/cad_categoria/$1";
/*SUB-CATEGORIAS*/
$route['ctrl/subcategorias'] = "home/list_subcategorias";
$route['ctrl/cad-subcategoria'] = "home/cad_subcategoria";
$route['ctrl/cad-subcategoria/(:any)'] = "home/cad_subcategoria/$1";
$route['ctrl/get-subcategoria'] = "home/get_subcategoria";
/* PRATOS */
$route['ctrl/pratos'] = "home/list_pratos";
$route['ctrl/cad-pratos'] = "home/cad_pratos";
$route['ctrl/cad-pratos/(:any)'] = "home/cad_pratos/$1";

/* LOGOUT */
$route['logout'] = "home/logout";



/* End of file routes.php */
/* Location: ./application/config/routes.php */