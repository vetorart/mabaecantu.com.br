<div class="centraliza mb60">
	<div class="row">
		<div class="col s12">
			<div class="titles uppercase">
				<h1>O Escritório</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m7">
			<p class="txt">
				Por meio de profissionais especializados nas áreas do direito criminal, direito civil e direito desportivo o escritório Maba & Cantú Advogados, atua na defesa, consultoria e acompanhamento de processos e procedimentos, tanto na esfera estadual quanto na federal, em todo território nacional. <br><br>

				Além disso, contamos com parceiros de grande porte, os quais atuam nas áreas do direito trabalhista, bancário e tributário, visando assim atender todas as demandas do escritório e satisfazer as necessidades de cada cliente. <br><br>

				O atendimento pessoal e especialista são características primordiais ao exercício da advocacia prestada por nossa equipe. <br><br>

				Nossa banca compõe-se deste modo, por advogados com grande formação técnica, cada qual em sua área de atuação, com envolvimento no meio acadêmico, fato que implica maior qualidade na atuação defensiva. <br><br>

				O escritório Maba & Cantú Advogados tem como principal objetivo o zelo pela liberdade e direitos de seus clientes, por meio da prestação de um serviço comprometido, responsável, ético e eficiente, a fim de alcançar-se o máximo sucesso nas causas que lhe são outorgadas. <br><br>
			</p>
			<div align="center">
				<a href="<?php echo base_url();?>equipe" class="btn">CONHEÇA NOSSA EQUIPE</a>
			</div>
		</div>
		<div class="col hide-on-small-only m5 mt20" align="center">
			<img src="<?php echo base_url();?>images/escritorio.jpg" alt="O Escritório" class="responsive-img">
		</div>
	</div>
</div>