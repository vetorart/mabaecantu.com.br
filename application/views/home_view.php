<div class="centraliza">
   <div class="row">
      <div class="col s12 m7 l8">
         <H5>BEM VINDO!</h5>
         <p class="txt">
            Prezao cliente, este será o seu painel para gerenciamento do conteúdo do seu site. <br>
            <strong>AVISO IMPORTANTE:</strong> Não forneça seu usuário e senha a terceiros.
         </p>
      </div>
      <div class="col s12 m5 l4">
         <h5>DÚVIDAS?</h5>
         <img src="<?php echo base_url()?>images/icon-atendimento.png" alt="" class="left responsive img">
         <div class="left txt">
            <strong class="telh">41 3011.3378</strong><br>
            Seg. à Sex: 09h às 18h <br>
            <a href="http://vetorart.com.br" target="_blank">vetorart.com.br</a>
         </div>
      </div>
      <div class="clear"></div>
   </div>
</div>