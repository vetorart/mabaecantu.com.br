<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<div class="titles uppercase">
				<span>Áreas de atuação</span>
				<h1>Direito Desportivo</h1>
			</div>
			<p class="txt">
				O Escritório Maba & Cantú Advocacia, desenvolve suas atividades profissionais de modo especializado em Direito Desportivo, atuando em todos os seus seguimentos, tais como direito do atleta, direito do intermediador, direitos do clube, interesses das empresas, direito de arena. <br><br>

				Dentre as atividades da advocacia desempenhada pelo Núcleo Desportivo, destacam-se: 
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m6">
			<p class="uppercase barbottom redtxt">Negociação, elaboração e revisão de contratos:</p>
			<p class="txtserv mb20 w85">
				•  Contrato de trabalho de atleta. <br>
				•  Contrato de representação e gerenciamento de carreira (Intermediário/Atleta). <br>
				•  Contrato de cessão de Direito de Imagem. <br> 
				•  Contrato de cessão de Direitos Econômicos. <br>
				•  Contrato de Patrocínio Esportivo e Publicidade. <br>
				•  Direito de Arena. 
			</p>
			<p class="uppercase barbottom redtxt">Assessoria Jurídica Especializada:</p>
			<p class="txtserv mb20 w85">
				•  Transferências nacionais e internacionais de atletas. <br> 
				•  Indenização por formação – Âmbito nacional e internacional. <br> 
				•  Mecanismo de Solidariedade <br>
				•  Assessoria a Clubes, Ligas, Federações e Confederações esportivas. <br> 
				•  Assessoria a Empresários/Intermediários e Atletas profissionais e não profissionais. 
			</p>
		</div>
		<div class="col s12 m6">
			<p class="uppercase barbottom redtxt">Defesas nos Tribunais e Órgãos Disciplinares Desportivos:</p>
			<p class="txtserv mb20">
				•  Defesas em Tribunais de Justiça Desportiva – TJD  <br>
				•  Defesas no Superior Tribunal de Justiça Desportiva – STJD (Diversas modalidades). <br>
				•  Comitê de Resolução de Litígios da CBF. <br>
				•  Câmara Nacional de Resolução de Disputas da CBF. <br>
				•  Câmara de Resolução de Disputas da FIFA. <br>
				•  Tribunal Arbitral do Esporte – TAS/CAS. <br>
				•  Tribunal Arbitral del Fútbol Sudamericano – TAFS <br>
				•  Litígios nacionais e Internacionais envolvendo o Doping. <br>
				•  Defesas no Tribunal Disciplinar da Confederación Sudamericana de Fútbol –CONMEBOL.  
			</p>
			<p class="uppercase barbottom redtxt">Propriedade Intelectual:</p>
			<p class="txtserv mb20">
				•  Procedimentos Administrativos e Judiciais. <br>
				•  Registro e Proteção de Marcas. 
			</p>
		</div>
	</div>
</div>