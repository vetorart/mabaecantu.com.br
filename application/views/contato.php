<div>
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<div class="titles uppercase mb40">
					<h1>Entre em Contato</h1>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<form class="formHome" action="<?php echo base_url(); ?>enviar-email" method="POST">
				<input class="badjust" type="hidden" name="urlAtual" value="<?php echo $urlAtual; ?>">
					<div class="row no-margin-bottom">
						<div class="col s12 m5">
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="nome" placeholder="NOME">
							</div>
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="email" placeholder="E-MAIL">
							</div>
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="cidade" placeholder="CIDADE">
							</div>
						</div>
						<div class="col hide-on-small-only m1">&nbsp;</div>
						<div class="col s12 m6">
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="telefone" placeholder="TELEFONE">
							</div>
							<div class="input-field no-margin-top col s12">
								<textarea name="mensagem" class="materialize-textarea" placeholder="Mensagem:"></textarea>
							</div>
							<div align="center">
								<button class="btn btnblack">ENVIAR</button>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="centraliza">
	<div class="row">
		<div class="col s12 m6">
			<div class="col s12 m7 linecontacts">
				<strong>Horário de atendimento</strong>
				<p>
					<span>Segunda á Sexta</span>
					Das 09h ás 18h
				</p>
			</div>
			<div class="col s12 m5 linecontacts">
				<strong>Telefones</strong>
				<p>
					<span>Dra. Mariana C. Cantú</span>
					(41) 99187-7447 <br>
					<span>Dr. Djonathan Maba</span>
					(41) 99704-4404 <br>
					<span>Escritório</span>	
					(41) 3347-1742
				</p>
			</div>
			<div class="clear"></div>
		</div>
		<div class="col s12 m6 linecontacts">
			<strong>Endereço</strong>
			<p>
				<span>Rua Desembargador Cid Campelo, 3596, Curitiba - PR</span>
			</p>
		</div>
		<div class="clear"></div>
	</div>
</div>


<div class="mapa">
	<?php echo $map['js']; ?>
	<?php echo $map['html']; ?>
</div>