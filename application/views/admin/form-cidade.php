
<script>
  $(document).ready(function() {
    $('select').material_select();
  });
</script>
<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Cidade</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/cidades">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php

    $id = null;
    $nome = null;
    $telefone = null;
    $delivery = null;
    $uf = null;
    $status = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php
    $id = $dados->id;
    $nome = $dados->nome;
    $telefone = $dados->telefone;
    $delivery = $dados->delivery;
    $uf = $dados->uf;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

  <?php echo form_open_multipart('home/salvar_cidade'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m6">
        <input name="nome" value="<?php echo $nome; ?>" type="text" class="validate">
        <label for="cidade">Cidade:</label>
      </div>
      <div class="input-field col s12 m2">
        <input name="uf" value="<?php echo $uf; ?>" type="text" class="validate">
        <label for="uf">UF:</label>
      </div>
      <div class="input-field col s12 m4">
        <input name="telefone" value="<?php echo $telefone; ?>" type="text" class="validate">
        <label for="telefone">Telefone:</label>
      </div>
      <div class="input-field col s12 m8">
        <input name="delivery" value="<?php echo $delivery; ?>" type="text" class="validate">
        <label for="delivery">URL Delivery:</label>
      </div>
      <div class="input-field col s12 m4">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <?php 
        if ($id == "") { ?>
      <button class="waves-effect waves-light btn">Cadastrar</button>
      <?php } else{ ?>
      <button class="waves-effect waves-light btn">Atualizar</button>
      <?php } ?>
    </div>   
  </form>
  
</div>