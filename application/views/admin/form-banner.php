<script>
  $(document).ready(function() {
    $('select').material_select();
  });
</script>

<?php

    $id = null;
    $nome = null;
    $url = null;
    $img = null;
    $imgmobile = null;
    $status = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $nome = $dados->nome;
    $url = $dados->url;
    $img = $dados->img;
    $imgmobile = $dados->imgmobile;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

  <?php

    /*
    foreach ($dados_subctg as $dados_subctg):

      $idcategoria = $dados_subctg->idcategoria;

    endforeach
    */

  ?>

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Banner</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/banners">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('banners/uploadbanner');?>
  <input name="id" value="<?php echo $id; ?>" type="hidden">
    <div class="row">
      <div class="input-field col s12 m4">
        <input name="nome" type="text" value="<?php echo $nome; ?>" class="validate">
        <label for="nome">Título:</label>
      </div>
      <div class="input-field col s12 m5">
        <input name="url" id="url" type="text" class="validate" value="<?php echo $url; ?>">
        <label for="url">URL sem http:</label>
      </div>
      <div class="input-field col s12 m3">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="clear"></div>      
    </div>
    <div class="row">      
      <div class="col s12 m6">
        <div class="file-field input-field">
          <div class="btn">
            <span>Imagem</span>
            <input type="file" name="img">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Selecione a imagem JPG ou PNG" value="<?php echo $img; ?>">
          </div>
        </div>
      </div>
      <div class="col s12 m6">
        <div class="file-field input-field">
          <div class="btn">
            <span>Imagem Mobile</span>
            <input type="file" name="imgmobile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Selecione a imagem JPG ou PNG" value="<?php echo $img; ?>">
          </div>
        </div>
      </div>
    </div>
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>