<script>
  $(document).ready(function() {
    $('select').material_select();
  });
</script>

<?php

    $id = null;
    $nome = null;
    $link = null;
    $status = null;
    $img = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $nome = $dados->nome;
    $link = $dados->link;
    $status = $dados->status;
    $img = $dados->img;
  ?>
  <?php endforeach ?>

  <?php

    /*
    foreach ($dados_subctg as $dados_subctg):

      $idcategoria = $dados_subctg->idcategoria;

    endforeach
    */

  ?>

  

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Imagem</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/galeria">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('home/upload_galeria');?>
  <input name="id" value="<?php echo $id; ?>" type="hidden">
    <div class="row">
      <div class="input-field col s12 m6">
        <input id="titulo" type="text" value="<?php echo $nome; ?>" class="validate">
        <label for="titulo">Título:</label>
      </div>
      <div class="input-field col s12 m6">
        <input name="url" id="url" type="text" class="validate">
        <label for="url">URL sem http:</label>
      </div>
      <div class="clear"></div>      
    </div>
    <div class="row">
      <div class="input-field col s12 m3 l2">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="col s12 m9 l10">
        <div class="file-field input-field">
          <div class="btn">
            <span>File</span>
            <input type="file" name="userfile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Selecione a imagem">
          </div>
        </div>
      </div>
    </div>
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>