
<script>
  $(document).ready(function() {
    $('select').material_select();
  });
</script>
<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Trocar Senha</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>home">Voltar</a>
  </div>
  <div class="clear"></div>

  

  <?php foreach ($dados as $dados): ?>
  <?php
    $id = $dados->id;
    $email = $dados->email;
    $usuario = $dados->usuario;
    $senha = $dados->senha;
  ?>
  <?php endforeach ?>

  <?php echo form_open_multipart('home/salvar_nova_senha'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m6">
        <input name="email" value="<?php echo $email; ?>" type="email" class="validate">
        <label for="cidade">E-mail:</label>
      </div>
      <div class="input-field col s12 m2">
        <input name="usuario" value="<?php echo $usuario; ?>" type="text" class="validate">
        <label for="usuario">Usuário:</label>
      </div>
      <div class="input-field col s12 m4">
        <input name="senha" type="password" class="validate">
        <label for="senha">Nova Senha:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Atualizar</button>
    </div>   
  </form>
  
</div>