<script>
   $(document).ready(function() {
    $('select').material_select();
    $("#valor").maskMoney();    
  });
</script>

<?php

    $id = null;
    $nome = null;
    $idcidade = null;
    $valor = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $nome = $dados->nome;
    $idcidade = $dados->idcidades;
    $valor = $dados->valor;
  ?>
  <?php endforeach ?>

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Rodízio e Buffet</h5>
  </div>
  <div class="col s12 m3 l2">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/rodizio">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('home/salvar_rodizio'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m4">
      <select name="idcidades">
        <option>Selecione</option>
        <?php foreach ($cidades as $cidades): ?>
          <option value="<?php echo $cidades->id; ?>" <?php if($idcidade==$cidades->id){ echo ' selected'; } ?>>
            <?php echo $cidades->nome; ?>
        </option>
        <?php endforeach ?>
      </select>
    <label>Cidade:</label>
      </div>
      <div class="input-field col s12 m4">
        <input name="nome" id="nome" type="text" value="<?php echo $nome; ?>" class="validate">
        <label for="nome">Item:</label>
      </div>
      <div class="input-field col s12 m4">
        <input name="valor" id="valor" type="text" value="<?php echo $valor; ?>" class="validate">
        <label for="valor">Preço sem R$:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>


