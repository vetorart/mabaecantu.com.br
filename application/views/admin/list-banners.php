
<script>
	$(document).ready(function(){
	    $('.tooltipped').tooltip({delay: 50});
	});
</script>


<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> BANNERS</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-banner" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="fa fa-plus"></i></a>
	</div>

	<div class="clear"></div>

	<table id="tabela">
		<thead>
			<tr class="tablehead">
				<th class="hide-on-small-only">Preview</th>
				<th>Banner</th>
				<th>Status</th>
				<th>Ação</th>
			</tr>
			<tr class="subhead">
				<th class="hide-on-small-only">&nbsp;</th>
				<th><input class="search" type="text" id="txtColuna2" placeholder="Filtrar:"/></th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>				
		</thead>
		<tbody>
			<?php foreach ($dados as $dados): ?>
			<tr class="contentr">
				<td class="hide-on-small-only"><?php if ($dados->img == "") { $dados->img = "../no-photo.jpg"; } ?><img src="<?php echo base_url().'images/banners/'.$dados->img;?>" alt="" class="responsive-img"></td>
				<td><?php echo $dados->nome; ?></td>
				<td><?php echo $dados->status; ?></td>
				<td>
					<div class="col s6">
						<a href="<?php echo base_url()?>ctrl/cad-banner/<?php echo $dados->id; ?>" title="Editar">
							<span class="fa fa-pencil"></span>
						</a>
					</div>
					<div class="col s6">
						<a href="<?php echo base_url()?>banners/deletar_banner/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar o Banner: <?php echo $dados->nome?>')" title="Excluir">
							<span class="fa fa-trash"></span>
						</a>
					</div>
				</td>
			</tr>
			<?php endforeach ?>	
		</tbody>
	</table>

	
</div>