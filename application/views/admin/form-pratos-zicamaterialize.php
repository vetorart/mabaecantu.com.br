<script>
   $(document).ready(function() {
    $('select').material_select();


    $('#idcategoria').on('change', function() {
      
alert('oi');
      $.ajax({
          type:"POST",
          dataType: 'json',
          url:"<?php echo base_url()?>home/get_subcategoria",
          data: {ctg:$('#idcategoria').val()},
          success: function(data) {

            $.each(data, function(item) {
                $("<option />").val(data[item].id)
                               .text(data[item].nome)
                               .appendTo($('select#idsubcategoria'));
            });
        

            $.each(data, function(i, data){
              $('#select-options-ffe5d240-1de6-f6ac-0e21-5c0b929d06c8').text('<option value="'+data.id+'">'+data.nome+'</option>');
              console.log('<option value="'+data.id+'">'+data.nome+'</option>');
            });

          }
        });

      
     });
  });
</script>

<?php

    $id = null;
    $titulo = null;
    $idcategoria = null;
    $idsubcategoria = null;
    $descricao = null;
    $status = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $titulo = $dados->titulo;
    $idsubcategoria = $dados->idsubcategoria;
    $descricao = $dados->descricao;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

  <?php

    foreach ($dados_subctg as $dados_subctg):

      $idcategoria = $dados_subctg->idcategoria;

    endforeach

  ?>

  

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Pratos</h5>
  </div>
  <div class="col s12 m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/pratos">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('home/salvar_prato'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      
      <div class="input-field col s12 m6">
      <select name="idcategoria" id="idcategoria">
        <option>Selecione</option>
        <?php foreach ($categorias as $categorias): ?>
          <option value="<?php echo $categorias->id; ?>" <?php if($idcategoria==$categorias->id){ echo ' selected'; } ?>>
            <?php echo $categorias->nome; ?>
        </option>
        <?php endforeach ?>
      </select>
      <label>Categoria:</label>
      </div>

      <div class="input-field col s12 m4">
      <select class="selectmaterial" name="idsubcategoria" id="idsubcategoria">
        <option>Selecione</option>
        <?php #foreach ($subcategorias as $subcategorias): ?>
            <!-- <option value="<?php #echo $subcategorias->id; ?>" <?php #if($idsubcategoria==$subcategorias->id){ #echo ' selected'; } ?>> -->
            <?php #echo $subcategorias->nome; ?>
        </option>
        <?php #endforeach ?>
      </select>
      <label>Sub-Categoria:</label>
      </div>

      <div class="input-field col s12 m5">
        <input name="titulo" id="titulo" type="text" value="<?php echo $titulo; ?>" class="validate">
        <label for="titulo">Título:</label>
      </div>
      <div class="input-field col s12 m3">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="input-field col s12 m6">
        <textarea class="materialize-textarea" name="descricao" id="" cols="30" rows="10"><?php echo $descricao ?></textarea>
        <label for="titulo">Descrição:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>


