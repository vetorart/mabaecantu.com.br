
<script>
	$(document).ready(function(){
	    $('.tooltipped').tooltip({delay: 50});
	});
</script>


<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> NOTÍCIAS</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-noticia" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="fa fa-plus"></i></a>
	</div>

	<div class="clear"></div>

	<table id="tabela">
		<thead>
			<tr class="tablehead">
				<th class="hide-on-small-only">Preview</th>
				<th>Título</th>
				<th>Status</th>
				<th>Ação</th>
			</tr>
			<tr class="subhead">
				<th class="hide-on-small-only">&nbsp;</th>
				<th><input class="search" type="text" id="txtColuna2" placeholder="Filtrar:"/></th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>				
		</thead>
		<tbody>
			<?php foreach ($dados as $dados): ?>
			<tr class="contentr">
				<td class="hide-on-small-only"><img src="<?php echo base_url().'images/noticias/'.$dados->img;?>" alt="" class="responsive-img"></td>
				<td><?php echo $dados->titulo; ?></td>
				<td><?php echo $dados->status; ?></td>
				<td>
					<div class="col s6">
						<a href="<?php echo base_url()?>ctrl/cad-noticia/<?php echo $dados->id; ?>" title="Editar">
							<span class="fa fa-pencil"></span>
						</a>
					</div>
					<div class="col s6">
						<a href="<?php echo base_url()?>noticias/deletar_noticia/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar a Notícia: <?php echo $dados->titulo?>')" title="Excluir">
							<span class="fa fa-trash"></span>
						</a>
					</div>
				</td>
			</tr>
			<?php endforeach ?>	
		</tbody>
	</table>

	
</div>