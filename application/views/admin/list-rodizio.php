
<script>
	$(document).ready(function(){
	    $('.tooltipped').tooltip({delay: 50});
	});
</script>


<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> RODÍZIO / BUFFET</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-rodizio" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="fa fa-plus"></i></a>
	</div>

	<div class="clear"></div>

	<table id="tabela">
		<thead>
			<tr class="tablehead">
				<th>Cidade</th>
				<th>Nome</th>
				<th class="hide-on-small-only">Preço</th>
				<th>Ação</th>
			</tr>
			<tr class="subhead">
				<th><input class="search" type="text" id="txtColuna1" placeholder="Filtrar:"/></th>
				<th><input class="search" type="text" id="txtColuna2" placeholder="Filtrar:"/></th>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
			</tr>				
		</thead>
		<tbody>
			<?php foreach ($dados as $dados): ?>
			<tr class="contentr">
				<td><?php echo $dados->cidade; ?></td>
				<td><?php echo $dados->nome; ?></td>
				<td class="hide-on-small-only"><?php echo 'R$ '.number_format($dados->valor, 2, ',', '.'); ?></td>
				<td>
					<div class="col s6">
						<a href="<?php echo base_url()?>ctrl/cad-rodizio/<?php echo $dados->id; ?>" title="Editar">
							<span class="fa fa-pencil"></span>
						</a>
					</div>
					<div class="col s6">
						<a href="<?php echo base_url()?>home/deletar_rodizio/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar o ítem: <?php echo $dados->nome.' - R$ '.$dados->valor; ?>')" title="Excluir">
							<span class="fa fa-trash"></span>
						</a>
					</div>
				</td>
			</tr>
			<?php endforeach ?>	
		</tbody>
	</table>

	<?php /* 
	<div class="titlelist">
		<div class="col m2 l2">
			Preview
		</div>
		<div class="col m2 l4">
			Título
		</div>
		<div class="col m3 l4">
			URL
		</div>
		<div class="col m3 l1">
			STATUS
		</div>
		<div class="col m2 l1" align="center">
			AÇÃO
		</div>
		<div class="clear"></div>
	</div>
	<?php foreach ($dados as $dados): ?>
	<div class="listbanners">
		<div class="llb">
			<div class="col s12 m2 l2">
				<img src="<?php echo base_url().'images/banners/'.$dados->img;?>" alt="" class="responsive-img">
			</div>
			<div class="col s12 m2 l4">
				<?php echo $dados->nome; ?>
			</div>
			<div class="col s12 m3 l4">
				<a href="http://<?php echo $dados->link; ?>" target="_blank"><?php echo $dados->link; ?></a>
			</div>
			<div class="col s12 m3 l1">
				<?php echo $dados->status; ?>
			</div>
			<div class="col s12 m2 l1">
				<div class="col s6">
					<a href="<?php echo base_url()?>ctrl/cad-banner/<?php echo $dados->id; ?>" title="Editar">
						<span class="fa fa-pencil"></span>
					</a>
				</div>
				<div class="col s6">
					<a href="<?php echo base_url()?>home/deletar_banner/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar o Banner: <?php echo $dados->nome?>')" title="Excluir">
						<span class="fa fa-trash"></span>
					</a>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php endforeach ?>
	*/ ?>
</div>