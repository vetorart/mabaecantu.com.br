<script>
  $(document).ready(function() {
    $('select').material_select();
    $('textarea#description').characterCounter();
  });
</script>

<?php

    $id = null;
    $titulo = null;
    $description = null;
    $bdesc = null;
    $slug = null;
    $img = null;
    $texto = null;
    $status = null;
    $tags = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $titulo = $dados->titulo;
    $description = $dados->description;
    $bdesc = $dados->bdesc;
    $slug = $dados->slug;
    $img = $dados->img;
    $texto = $dados->texto;
    $status = $dados->status;
    $tags = $dados->tags;
  ?>


  <?php endforeach ?>

  <?php

    /*
    foreach ($dados_subctg as $dados_subctg):

      $idcategoria = $dados_subctg->idcategoria;

    endforeach
    */

  ?>

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Notícia</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/noticias">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('noticias/salvar_noticia');?>
  <input type="hidden" name="datacad" value="<?php echo date('d/m/Y'); ?>">
  <input name="id" value="<?php echo $id; ?>" type="hidden">
    <div class="row">
      <div class="input-field col s12 m6">
        <input name="titulo" type="text" value="<?php echo $titulo; ?>" class="validate">
        <label for="titulo">Título:</label>
      </div>
      <div class="input-field col s12 m6">
        <input name="tags" id="tags" type="text" class="validate" value="<?php echo $tags; ?>">
        <label for="tags">Tags:</label>
      </div>
      <div class="clear"></div>      
    </div>
    <div class="row">
      <div class="input-field col s12 m3 l2">
        <select name="status">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="col s12 m9 l10">
        <div class="file-field input-field">
          <div class="btn">
            <span>File</span>
            <input type="file" name="userfile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Selecione a imagem" value="<?php echo $img; ?>">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
            <textarea id="textarea1" name="description" class="materialize-textarea" length="120"><?php echo $description; ?></textarea>
            <label for="textarea1">Description:</label>
        </div>
        <div class="input-field col s6">
            <textarea id="textarea1" name="bdesc" class="materialize-textarea" data-length="120"><?php echo $bdesc; ?></textarea>
            <label for="textarea1">Descrição Breve:</label>
        </div>
      </div>
      <div class="input-field col s12">
        <label for="texto">Notícia</label> <br>
        <textarea name="texto" id="texto" class="materialize-textarea editor">
          <?php echo $texto; ?>
        </textarea>
      </div>
    </div>
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>

<script type="text/javascript">
        var toolbar = [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['undo', ['undo', 'redo']],
            ['misc', ['link', 'hr', 'codeview']],
            ['para', ['ul', 'ol', 'outdentButton', 'indentButton']],
        ];

        $('.editor').summernote({
            toolbar: toolbar,
            height: 250,
            minHeight: 100,
            defaultBackColor: '#fff'
        });
    </script> 