<div class="centraliza">
	<div class="row">
		<div class="col s12 m8 l9">
			<h1>Bem vindo!</h1>
			<p class="txt">
				Prezado cliente, indicamos que não repasse seu usuário e senha a terceiros, pois este painel é responsável pelo gerenciamento do conteúdo do seu site. <br>
				Em caso de dúdivas, entre em contato pelo telefone (41) 3011.3378 ou pelo e-mail atendimento@vetorart.com.br estamos atendendo de seg. a sex. das 09h às 18h e aos sábados das 10h às 14h somente por e-mail.
			</p>
		</div>
	</div>
</div>