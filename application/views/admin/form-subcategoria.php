<script>
   $(document).ready(function() {
    $('select').material_select();
  });
</script>

<?php

    $id = null;
    $nome = null;
    $categoria = null;
    $subtitulo = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $nome = $dados->nome;
    $categoria = $dados->idcategoria;
    $status = $dados->status;
    $subtitulo = $dados->subtitulo;
  ?>
  <?php endforeach ?>

<div class="row centraliza">
  <div class="right mb20">
    <a class="btnvoltar" href="<?php echo base_url()?>ctrl/subcategorias">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('home/salvar_subcategoria'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m4">
      <select name="idcategoria">
        <option>Selecione</option>
        <?php foreach ($categorias as $categorias): ?>
          <option value="<?php echo $categorias->id; ?>" <?php if($categoria==$categorias->id){ echo ' selected'; } ?>>
            <?php echo $categorias->nome; ?>
        </option>
        <?php endforeach ?>
      </select>
    <label>Categoria:</label>
      </div>
      <div class="input-field col s12 m4">
        <input name="nome" id="titulo" type="text" value="<?php echo $nome; ?>" class="validate">
        <label for="titulo">Sub-Categoria:</label>
      </div>
      <div class="input-field col s12 m4">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>

      <div class="input-field col s12">
        <textarea name="subtitulo" id="textarea1" class="materialize-textarea"><?php echo $subtitulo; ?></textarea>
        <label for="textarea1">Descrição</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>


