
<script>
	$(document).ready(function(){
	    $('.tooltipped').tooltip({delay: 50});
	});
</script>


<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> VÍDEOS</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-video" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="fa fa-plus"></i></a>
	</div>

	<div class="clear"></div>

	<table id="tabela">
		<thead>
			<tr class="tablehead">
				<th>Status</th>
				<th>Título</th>
				<th>Ação</th>
			</tr>
			<tr class="subhead">
				<th class="hide-on-small-only">&nbsp;</th>
				<th><input class="search" type="text" id="txtColuna2" placeholder="Filtrar:"/></th>
				<th>&nbsp;</th>
			</tr>				
		</thead>
		<tbody>
			<?php foreach ($dados as $dados): ?>
			<tr class="contentr">
				<td><?php echo $dados->status; ?></td>
				<td><?php echo $dados->titulo; ?></td>
				<td>
					<div class="col s6">
						<a href="<?php echo base_url()?>ctrl/cad-video/<?php echo $dados->id; ?>" title="Editar">
							<span class="fa fa-pencil"></span>
						</a>
					</div>
					<div class="col s6">
						<a href="<?php echo base_url()?>ctrl/videos/deletar/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar o vídeo: <?php echo $dados->titulo?>')" title="Excluir">
							<span class="fa fa-trash"></span>
						</a>
					</div>
				</td>
			</tr>
			<?php endforeach ?>	
		</tbody>
	</table>

	
</div>