
<script>
  $(document).ready(function() {
    $('select').material_select();
  });
</script>
<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Categoria</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/categorias">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php

    $id = null;
    $nome = null;
    $slug = null;
    $status = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php
    $id = $dados->id;
    $nome = $dados->nome;
    $slug = $dados->slug;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

  <?php echo form_open_multipart('home/salvar_categoria'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m9">
        <input name="nome" value="<?php echo $nome; ?>" type="text" class="validate">
        <label for="categoria">Categoria:</label>
      </div>
      <div class="input-field col s12 m3">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <?php 
        if ($id == "") { ?>
      <button class="waves-effect waves-light btn">Cadastrar</button>
      <?php } else{ ?>
      <button class="waves-effect waves-light btn">Atualizar</button>
      <?php } ?>
    </div>   
  </form>
  
</div>