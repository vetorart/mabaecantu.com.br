<script>
   $(document).ready(function() {
    $('select').material_select();
    $("#valor").maskMoney();    
  });
</script>

<?php

    $id = null;
    $idcidade = null;
    $idprato = null;
    $valor = null;
    $status = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php


    $id = $dados->id;
    $idcidade = $dados->idcidade;
    $idprato = $dados->idprato;
    $valor = $dados->valor;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Ítens do cardápio</h5>
  </div>
  <div class="col s12 m3 l2">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/rodizio">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('home/salvar_cardapio'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m4">
        <select name="idcidade">
          <option>Selecione</option>
          <?php foreach ($cidades as $cidades): ?>
            <option value="<?php echo $cidades->id; ?>" <?php if($idcidade==$cidades->id){ echo ' selected'; } ?>>
              <?php echo $cidades->nome; ?>
          </option>
          <?php endforeach ?>
        </select>
        <label>Cidade:</label>
      </div>

      <div class="input-field col s12 m4">
        <select name="idprato">
          <option>Selecione</option>
          <?php foreach ($pratos as $pratos): ?>
            <option value="<?php echo $pratos->id; ?>" <?php if($idprato==$pratos->id){ echo ' selected'; } ?>>
              <?php echo $pratos->titulo; ?>
          </option>
          <?php endforeach ?>
        </select>
        <label>Prato:</label>
      </div>

      <div class="input-field col s12 m4">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>

      <div class="input-field col s12 m4">
        <input name="valor" id="valor" type="text" value="<?php echo $valor; ?>" class="validate">
        <label for="valor">Preço sem R$:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>


