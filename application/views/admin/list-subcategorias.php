<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> SUB-CATEGORIAS</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-subcategoria" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="material-icons">+</i></a>
	</div>
	<div class="clear"></div>

	<div id="msg">
		<?php 
		
			if( isset($msg) && $msg != "0" ){
				switch ($msg) {
					case 'error_tem_pra':
						echo 'Não foi possível excluir esta Sub-Categoria, pois existem Pratos relacionadas a ela!';
						break;
						
					case 'error_tem_car':
						echo 'Não foi possível excluir esta Sub-Categoria, pois existem Cardapios relacionados a ela!';
						break;
					
					default:
						# code...
						break;
				}
			}

		?>
	</div>
	<div class="titlelist">
		<div class="col m4 l4">
			Categoria
		</div>
		<div class="col m4 l4">
			Sub-Categoria
		</div>
		<div class="col m2 l2">
			STATUS
		</div>
		<div class="col m2 l2" align="center">
			AÇÃO
		</div>
		<div class="clear"></div>
	</div>
	<?php foreach ($dados as $dados): ?>
	<div class="listbanners">
		<div class="llb">
			<div class="col s12 m4">
				<?php echo $dados->categoria; ?>
			</div>
			<div class="col s12 m4">
				<?php echo $dados->nome; ?>
			</div>
			<div class="col s12 m2">
				<?php echo $dados->status; ?>
			</div>
			<div class="col s12 m2">
				<div class="col s6">
					<a href="<?php echo base_url()?>ctrl/cad-subcategoria/<?php echo $dados->id; ?>" title="Editar">
						<span class="fa fa-pencil"></span>
					</a>
				</div>
				<div class="col s6">
					<a href="<?php echo base_url()?>home/deletar_subcategoria/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar a categoria <?php echo $dados->nome?>')" title="Excluir">
						<span class="fa fa-trash"></span>
					</a>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php endforeach ?>
</div>