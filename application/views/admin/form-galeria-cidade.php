<script>
   $(document).ready(function() {
    $('select').material_select();
  });
</script>

<?php

    $id = null;
    $idcidade = null;
    $titulo = null;
    $categoria = null;
    $img = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $idcidade = $dados->idcidade;
    $titulo = $dados->titulo;
    $img = $dados->img;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar imagem</h5>
  </div>
  <div class="col s12 m3 l2">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/galeria">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('home/upload_galeria_cidade'); ?>
    <input name="id" value="<?php echo $id; ?>" type="hidden">

    <div class="row">
      <div class="input-field col s12 m4">
      <select name="idcidade">
        <option>Selecione</option>
        <?php foreach ($cidades as $cidades): ?>
          <option value="<?php echo $cidades->id; ?>" <?php if($idcidade==$cidades->id){ echo ' selected'; } ?>>
            <?php echo $cidades->nome; ?>
        </option>
        <?php endforeach ?>
      </select>
    <label>Cidade:</label>
      </div>
      <div class="input-field col s12 m8">
        <input name="titulo" id="titulo" type="text" value="<?php echo $titulo; ?>" class="validate">
        <label for="titulo">Título:</label>
      </div>
      <div class="col s12 m8">
        <div class="file-field input-field">
          <div class="btn">
            <span>File</span>
            <input type="file" name="userfile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Selecione a imagem" value="<?php echo $img; ?>">
          </div>
        </div>
      </div>
      <div class="input-field col s12 m4">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="clear"></div>      
    </div>
      
    <div class="row s12 right">
      <button class="waves-effect waves-light btn">Cadastrar</button>
    </div>   
  </form>
</div>


