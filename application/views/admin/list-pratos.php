<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> PRATOS</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-pratos" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="material-icons">+</i></a>
	</div>
	<div class="clear"></div>
	<div id="msg">
		<?php 

			if( isset($msg) && $msg != "0" ){
				switch ($msg) {
					case 'error_tem_prc':
						echo 'Não foi possível excluir este Prato, pois o mesmo está cadastrado em um cardápio!';
						break;
					
					default:
						# code...
						break;
				}
			}
			
		?>
	</div>
	<div class="titlelist hide-on-small-only">
		<div class="col m4">
			Categoria / Sub-Categoria
		</div>
		<div class="col m4">
			Prato
		</div>
		<div class="col m2">
			STATUS
		</div>
		<div class="col m2" align="center">
			AÇÃO
		</div>
		<div class="clear"></div>
	</div>
	<?php foreach ($dados as $dados): ?>
	<div class="listbanners">
		<div class="llb">
			<div class="col s12 m4">
				<?php echo $dados->colcat; ?>
			</div>
			<div class="col s12 m4">
				<?php echo $dados->titulo; ?>
			</div>
			<div class="col s12 m2">
				<?php echo $dados->status; ?>
			</div>
			<div class="col s12 m2">
				<div class="col s6">
					<a href="<?php echo base_url()?>ctrl/cad-pratos/<?php echo $dados->id; ?>" title="Editar">
						<span class="fa fa-pencil"></span>
					</a>
				</div>
				<div class="col s6">
					<a href="<?php echo base_url()?>home/deletar_pratos/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar o prato <?php echo $dados->titulo?>')" title="Excluir">
						<span class="fa fa-trash"></span>
					</a>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<?php endforeach ?>
</div>