<script>
  $(document).ready(function() {
    $('select').material_select();
  });
</script>

<?php

    $id = null;
    $nome = null;
    $idcategoria = null;
    $slug = null;
    $video = null;
    $img = null;
    $tags = null;
    $bdesc = null;
    $description = null;
    $descricao = null;
    $especificacao = null;
    $garantia = null;
    $status = null;

  ?>

  <?php foreach ($dados as $dados): ?>
  <?php

    $id = $dados->id;
    $nome = $dados->nome;
    $idcategoria = $dados->idcategoria;
    $slug = $dados->slug;
    $video = $dados->video;
    $img = $dados->img;
    $tags = $dados->tags;
    $bdesc = $dados->bdesc;
    $description = $dados->description;
    $descricao = $dados->descricao;
    $especificacao = $dados->especificacao;
    $garantia = $dados->garantia;
    $status = $dados->status;
  ?>
  <?php endforeach ?>

  <?php

    /*
    foreach ($dados_subctg as $dados_subctg):

      $idcategoria = $dados_subctg->idcategoria;

    endforeach
    */

  ?>

<div class="row centraliza">
  <div class="col s12 m9 l10">
    <h5><span class="fa fa-angle-right"></span> Cadastrar Produto</h5>
  </div>
  <div class="col hide-on-small-only m3 l2 mb20">
    <a class="btnvoltar right" href="<?php echo base_url()?>ctrl/produtos">Voltar</a>
  </div>
  <div class="clear"></div>

  <?php echo form_open_multipart('produtos/salvar_produto');?>
  <input name="id" value="<?php echo $id; ?>" type="hidden">
    <div class="row">
      <div class="input-field col s12 m6">
        <input name="nome" type="text" value="<?php echo $nome; ?>" class="validate">
        <label for="titulo">Título:</label>
      </div>
      <div class="input-field col s12 m6">
        <input name="video" type="text" value="<?php echo $video; ?>" class="validate">
        <label for="titulo">URL VÌDEO YOUTUBE:</label>
      </div>
      <div class="input-field col s12 m12">
        <input name="tags" id="tags" type="text" class="validate" value="<?php echo $tags; ?>">
        <label for="tags">Tags:</label>
      </div>
      <div class="input-field col s12 m6">
        <textarea name="bdesc" id="bdesc" class="materialize-textarea"><?php echo $bdesc ?></textarea>
        <label for="bdesc">Descrição breve:</label>
      </div>
      <div class="input-field col s12 m6">
        <textarea name="description" id="description" class="materialize-textarea"><?php echo $description; ?></textarea>
        <label for="description">SEO Description:</label>
      </div>
      <div class="clear"></div>      
    </div>
    <div class="row">
      <div class="input-field col s12 m3 l2">
        <select name="status" id="">
          <?php if ($status == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="Ativo">Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Ativo"){ ?>
          <option value="Ativo" selected>Ativo</option>
          <option value="Inativo">Inativo</option>
          <?php } else if ($status == "Inativo"){ ?>
          <option value="Ativo">Ativo</option>
          <option value="Inativo" selected>Inativo</option>
          <?php } ?>
        </select>
        <label>Status:</label>
      </div>
      <div class="input-field col s12 m4 l3">
        <select name="idcategoria" id="">
          <?php if ($idcategoria == "") {?>
          <option disabled selected>Selecionar</option>
          <option value="1">Linha de Escritório</option>
          <option value="2">Linha de Produção</option>
          <?php } else if ($idcategoria == "1"){ ?>
          <option value="1" selected>Linha de Escritório</option>
          <option value="2">Linha de Produção</option>
          <?php } else if ($idcategoria == "2"){ ?>
          <option value="1">Linha de Escritório</option>
          <option value="2" selected>Linha de Produção</option>
          <?php } ?>
        </select>
        <label>Categoria:</label>
      </div>
      <div class="col s12 m5 l7">
        <div class="file-field input-field">
          <div class="btn">
            <span>File</span>
            <input type="file" name="userfile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Selecione a imagem" value="<?php echo $img; ?>">
          </div>
        </div>
      </div>
      <div>
        <br>
        <br>
        <br>
        <br>
      </div>
      <p>
        &nbsp;
      </p>
      <div class="input-field col s12">
        <label for="descricao">Descrição do Produto:</label> <br>
        <textarea name="descricao" id="descricao" class="materialize-textarea editor">
          <?php echo $descricao; ?>
        </textarea>
      </div>
      <div class="clear"></div>
      <div class="input-field col s12">
        <label for="especificacao">Especificação do Produto:</label> <br>
        <textarea name="especificacao" id="especificacao" class="materialize-textarea editor2"><?php echo $especificacao; ?></textarea>
      </div>
    </div>
      <div class="clear"></div>
      <div class="input-field col s12">
        <label for="garantia">Garantia do Produto:</label> <br>
        <textarea name="garantia" id="garantia" class="materialize-textarea editor3"><?php echo $garantia; ?></textarea>
      </div>
    </div>
    <div class="clear"></div>
    <br>
    <div class="centraliza">
      <div class="row">
        <div class="col s12">
          <button class="waves-effect waves-light btn right">Cadastrar</button>
        </div>
      </div>
    </div>   
  </form>
</div>

<script type="text/javascript">
        var toolbar = [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['undo', ['undo', 'redo']],
            ['misc', ['hr', 'codeview']],
            ['para', ['ul', 'ol', 'outdentButton', 'indentButton']],
        ];

        $('.editor').summernote({
            toolbar: toolbar,
            height: 250,
            minHeight: 100,
            defaultBackColor: '#000'
        });
        $('.editor2').summernote({
            toolbar: false,
            height: 250,
            minHeight: 100,
            defaultBackColor: '#000'
        });
        $('.editor3').summernote({
            toolbar: false,
            height: 250,
            minHeight: 100,
            defaultBackColor: '#000'
        });
    </script> 