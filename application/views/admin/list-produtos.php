
<script>
	$(document).ready(function(){
	    $('.tooltipped').tooltip({delay: 50});
	});
	function MM_jumpMenu(targ,selObj,restore){ //v3.0
	  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
	  if (restore) selObj.selectedIndex=0;
	}
</script>


<div class="row centraliza">
	<div class="col s12 m9 l10">
		<h5><span class="fa fa-angle-right"></span> PRODUTOS</h5>
	</div>
	<div class="col s12 m3 l2">
		<a href="<?php echo base_url()?>ctrl/cad-produto" class="btn-floating btn-large waves-effect waves-light red right" title="Adicionar Banner"><i class="fa fa-plus"></i></a>
	</div>

	<div class="clear"></div>

	<table id="tabela">
		<thead>
			<tr class="tablehead">
				<th class="hide-on-small-only">Preview</th>
				<th>Título</th>
				<th>Status</th>
				<th class="ordemcamp">ORDEM</th>
				<th>Ação</th>
			</tr>
			<tr class="subhead">
				<th class="hide-on-small-only">&nbsp;</th>
				<th><input class="search" type="text" id="txtColuna2" placeholder="Filtrar:"/></th>
				<th>&nbsp;</th>
				<th class="ordemcamp">&nbsp;</th>
				<th>&nbsp;</th>
			</tr>				
		</thead>
		<tbody>
			
			<?php foreach ($dados as $dados): ?>
			<tr class="contentr">
				<td class="hide-on-small-only"><img src="<?php echo base_url().'images/produtos/'.$dados->img;?>" alt="" class="responsive-img"></td>
				<td><?php echo $dados->nome; ?></td>
				<td><?php echo $dados->status; ?></td>
				<td class="ordemcamp">
					<form name="ordem">
					  <select class="browser-default orderlist" name="jumpMenu" onchange="MM_jumpMenu('parent',this,0)">
					  	<?php $pidatual = $dados->ordem; ?>
					  	<?php foreach ($order as $ord): ?>
					    	<option <?php if ($pidatual == $ord->ordem) { echo "selected"; } ?> value="<?php echo base_url().'ctrl/produtos/ordenar/'.$pidatual.'/'.$ord->ordem; ?>"><?php echo $ord->ordem; ?></option>
					  	<?php endforeach ?>

					  </select>
					</form>
				</td>
				<td>
					<div class="col s6">
						<a href="<?php echo base_url()?>ctrl/cad-produto/<?php echo $dados->id; ?>" title="Editar">
							<span class="fa fa-pencil"></span>
						</a>
					</div>
					<div class="col s6">
						<a href="<?php echo base_url()?>produtos/deletar_produto/<?php echo $dados->id; ?>" onclick="return confirm('Deseja deletar o produto: <?php echo $dados->nome?>')" title="Excluir">
							<span class="fa fa-trash"></span>
						</a>
					</div>
				</td>
			</tr>
			<?php endforeach ?>	
		</tbody>
	</table>

	
</div>