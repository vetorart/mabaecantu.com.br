<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<div class="titles uppercase">
				<span>Áreas de atuação</span>
				<h1>Criminal Individual / Empresarial</h1>
			</div>
			<p class="txt">
				O Escritório Maba & Cantú Advocacia, por meio de sua sócia Dra. Mariana Cantú, desenvolve suas atividades profissionais voltadas ao Direito Criminal, atuando na defesa dos interesses de acusados ou vítimas de crimes comuns e empresariais perante órgãos de classe, administrativos e judiciais, estaduais e federais. <br><br>

				Dentre as atividades do núcleo da Advocacia Criminal, destacam-se:  
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m6">
			<p class="uppercase barbottom redtxt">Tribunal do Júri:</p>
			<p class="txtserv mb20 w85">
				Homicídio; induzimento, instigação ou auxílio a suicídio; infanticídio; aborto. 
			</p>
			<p class="uppercase barbottom redtxt">Crimes Comuns:</p>
			<p class="txtserv mb20 w85">
				Contra o patrimônio, contra a honra (injúria/ calúnia/ difamação), contra a saúde pública, contra a dignidade sexual, contra a fé pública. 
			</p>
			<p class="uppercase barbottom redtxt">Crimes contra a Administração Pública: </p>
			<p class="txtserv mb20 w85">
				Cometidos por particulares ou funcionários públicos contra a Administração Pública.
			</p>
			<p class="uppercase barbottom redtxt">Direito Penal Ambiental:</p>
			<p class="txtserv mb20 w85">
				Crimes contra a Fauna, Flora e Poluição.
			</p>
			<p class="uppercase barbottom redtxt">Direito Penal Empresarial:</p>
			<p class="txtserv mb20 w85">
				Crimes contra a Ordem Econômica, Relações de Consumo, Crimes Fiscais, Lavagem de dinheiro e capitais, compliance.
			</p>
		</div>
		<div class="col s12 m6">
			<p class="uppercase barbottom redtxt">Direito Penal Eleitoral:</p>
			<p class="txtserv mb20">
				Crimes eleitorais; Corrupção; Inscrição Fraudulenta; Coação ou Ameaça; Concentração de Eleitores; Fraude de Voto; Divulgação de fatos inverídicos, etc.  
			</p>
			<p class="uppercase barbottom redtxt">Direito Penal Médico:</p>
			<p class="txtserv mb20">
				Erro médico; responsabilidade penal; assessoria e consultoria a hospitais.  
			</p>
			<p class="uppercase barbottom redtxt">Crimes Cibernéticos:</p>
			<p class="txtserv mb20">
				Crimes cometidos na internet.  
			</p>
			<p class="uppercase barbottom redtxt">Direito Penal Militar:</p>
			<p class="txtserv mb20">
				Defesa de Policiais Militares, membros do Corpo de Bombeiros Militar e das Forças Armadas, perante a Justiça Militar e Tribunal do Júri. 
			</p>
			<p class="uppercase barbottom redtxt">Execução da Pena:</p>
			<p class="txtserv mb20">
				Administração da pena, do início ao fim (livramento Condicional, progressão de regime, harmonização de regime, remissão, trabalho externo, saída temporária, indulto, transferências, entre outros).  
			</p>


		</div>
	</div>
</div>