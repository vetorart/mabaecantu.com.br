    <div class="centraliza">
      <div class="top25 bot20" align="center">
        <img class="img-responsive" src="<?php echo base_url() . 'images/logo-login.png' ?>" alt="">
      </div>

    <div class="advertencia">
      <?php echo validation_errors(); ?>
    </div>
      <div class="row box-login">
      <?php echo form_open('verifylogin/recovery'); ?>
        <div class="row">          
          <div class="input-field col s12">
            <input id="email" name="email" type="email" class="validate" required>
            <label for="usuario">E-mail cadastrado:</label>
          </div>
          <div class="col s8">
            <input type="submit" class="btn btn-success" value="Trocar Senha"/>
          </div>
          <div class="col s4">
            <a href="<?php echo base_url();?>login" class="lembrar">Voltar</a>
          </div>
        </div>
      </form>
    </div>
    </div>
<div class="direitoslogin">
  Todos os direitos reservados - 2015 <br>
  Desenvolvido por <a href="http://decolenaweb.com.br" target="_blank">Decole na Web</a>
</div>