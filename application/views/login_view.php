    <div class="centraliza">
      <div class="top25 bot20" align="center">
        <img class="img-responsive" src="<?php echo base_url() . 'images/logo-login.png' ?>" alt="">
      </div>

    <div class="advertencia">
      <?php echo validation_errors(); ?>
    </div>
      <div class="row box-login">
      <?php echo form_open('verifylogin'); ?>
        <div class="row">          
          <div class="input-field col s12">
            <input id="username" name="username" required type="text" class="validate">
            <label for="usuario">Usuário:</label>
          </div>
          <div class="input-field col s12">
            <input id="password" name="password" type="password" required class="validate">
            <label for="senha">Senha:</label>
          </div>
          <div class="col s6">
            <input type="submit" class="btn btn-success" value="Login"/>
          </div>
          <div class="col s6">
            <a href="<?php echo base_url();?>recuperar-senha" class="lembrar">Lembrar senha</a>
          </div>
        </div>
      </form>
    </div>
    </div>
<div class="direitoslogin">
  Todos os direitos reservados - 2015 <br>
  Desenvolvido por <a href="http://vetorart.com.br" target="_blank">VetorArt Design</a>
</div>