<div class="centraliza mb60">
	<div class="row">
		<div class="col s12">
			<div class="titles uppercase">
				<h1>Nossa Equipe</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- ocultado -->
		<div class="col hide s12" align="center">
			<img src="<?php echo base_url();?>images/mariana.jpg" alt="Mariana Cantú" class="responsive-img">
		</div>
		<div class="col s12 txtequipe">
			<div class="nome">Mariana Coelho Cantú - Advogada</div>
			<span class="especializacao">Núcleo Criminal</span>
			<p>
				<strong>Informações:</strong>
				* Especialização em Direito Penal e Direito Processual Penal pela Academia Brasileira de Direito Constitucional, Curitiba, Brasil. <br><br>

				Título: O Encarceramento sob a ótica da Penitenciária Feminina do Paraná – PFP. <br><br>

				* Graduação em Direito pela Universidade Positivo, Brasil. <br><br>

				Título: A Preservação da fauna e da flora silvestres brasileira sob a ótica do tráfico ilegal de plantas e animais silvestres no Brasil.
			</p>
		</div>
		<div class="col hide m5" align="center">
			<img src="<?php echo base_url();?>images/mariana.jpg" alt="Mariana Cantú" class="responsive-img">
		</div>
	</div>
	<div class="row">
		<!-- ocultado -->
		<div class="col hide s12" align="center">
			<img src="<?php echo base_url();?>images/djonathan.jpg" alt="Djonathan Maba" class="responsive-img">
		</div>
		<div class="col s12 txtequipe">
			<div class="nome">DJONATHAN PELICK MABA – Advogado</div>
			<span class="especializacao">Núcleo Cível/Empresarial</span>
			<p>
				<strong>Informações:</strong>
				- Especialização em Direito Civil e Direito Processo Civil.
				Centro Universitário Curitiba - UniCuritiba, Curitiba, Paraná. <br><br>
				Título: A execução à luz do Novo Código de Processo Civil e a desconsideração da personalidade Jurídica. <br><br>
				- Graduação em Direito: Universidade Positivo, Brasil. <br><br>
				Título: A Extrafiscalidade dos Impostos sobre Grandes Fortunas 
			</p>
		</div>
		<div class="col hide m5" align="center">
			<img src="<?php echo base_url();?>images/djonathan.jpg" alt="Djonathan Maba" class="responsive-img">
		</div>
	</div>
</div>