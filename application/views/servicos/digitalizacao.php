<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<h2 class="title tc">
				conhecimento combinado a tecnologia
			</h2>
			<p class="txt tc ct">
				Segurança, controle e confiabilidade nos resultados são aspectos que precisam ser considerados na Digitalização de Documentos. Além da experiência de 18 anos em projetos de digitalização, contamos com especialistas com certificação CDIA+ (Certified Document Imaging Architect Plus) preparados para atender a todo tipo de complexidade técnica.  <br>
				Moldamos nossas soluções de digitalização para se adaptarem à necessidade de qualquer cliente, independentemente do porte ou ramo de negócios, e da condição dos originais físicos. <br>
				Entenda como estabelecemos processos de digitalização que transformam lentidão e robustez, em agilidade e simplicidade para acessar a informação correta, no momento em que for necessária:

			</p>
		</div>
	</div>
</div>
<div class="blocks" id="dpdgf">
	<div class="centraliza">
		<div class="slider responsiveC">
			<div>
				<div class="row mb40">
					<div class="col s12">
						<h3>Digitalização de Documentos de Uso Administrativo</h3>
						<p>
							Combinamos conhecimento, modernas tecnologias de captura de imagem e escâneres de alto desempenho, em um processo especialmente desenhado por nossos especialistas. Os resultados são imagens digitais de qualidade e legibilidade indiscutíveis. <br>
							Dentre as principais características da Digitalização de Documentos Administrativos, estão:

						</p>
						<ul class="listagem">
							<li>
								O levantamento e análise de dados para definir o que vale a pena digitalizar;
							</li>
							<li>
								Preparo adequado dos originais físicos, com remoção de grampos, clipes e sujidades que interferem na boa qualidade da digitalização;
							</li>
							<li>
								Uso de softwares dedicados ao controle do processo de digitalização, capaz de representação fiel do conteúdo informacional;
							</li>
							<li>
								Uso de softwares de Reconhecimento Óptico de Caracteres, ou OCR (Optical Character Recognition), para tornar possível a busca por termos contidos no conteúdo, e eventual edição;
							</li>
							<li>
								Indexação dos documentos por quantos campos forem necessários.
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div>
				<div class="row">
					<div class="col s12">
						<h3>Digitalização para produção de Microfilmes e Microfichas</h3>
						<p>
							Praticamente inalteráveis, o microfilme e a microficha são as formas de armazenamento mais confiáveis para documentos quando o objetivo é sua preservação por longos períodos de tempo. 
							Através de uma tecnologia chamada COM (Computer Output to Microfilm), as imagens digitais são fixadas no microfilme ou microficha. A imagem pode ser miniaturizada em até 50 vezes, e o processo ainda permite a reconversão do documento microfilmado novamente em imagem digital, proporcionando segurança e agilidade nas pesquisas. <br>
							Dentre os principais benefícios da Digitalização para produção de Microfilmes e Microfichas, estão:
						</p>
						<ul class="listagem">
							<li>
								Evitar a perda dos documentos por deterioração;
							</li>
							<li>
								Dificultar a ação de falsificadores;
							</li>
							<li>
								Garantir durabilidade plena através da conformidade com normas internacionais de organizações como a ISO e ANSI/AIIM;
							</li>
							<li>
								Contornar problemas de obsolescência dos suportes digitais, já que a imagem do microfilme é analógica e o processo é conhecido no mundo todo.
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div>
				<div class="row mb40">
					<div class="col s12">
						<h3>Digitalização de Plantas e Desenhos de Grandes Formatos</h3>
						<p>
							Aspectos como cor, gramatura do papel, tamanho das plotagens (A2, A1, A0, etc) são determinantes para a digitalização de documentos de grandes formatos, principalmente aqueles a serem usados pelas áreas de Engenharia. <br> 
							A Núcleo Básico conta com modernos scanners de grandes formatos e técnicas para vetorização das imagens captadas, que asseguram a qualidade e fidelidade das imagens digitais transmitindo confiança aos profissionais de engenharia que usarão estas informações no dia a dia de suas profissões. O resultado da vetorização é um novo arquivo eletrônico em formato DWG ou similar, como um “redesenho” daquilo que foi digitalizado.
							Dentre os principais benefícios da Digitalização de Documentos em Grandes Formatos, estão:
						</p>
						<ul class="listagem">
							<li>
								Manuseio facilitado, com visualização por mais de uma pessoa ao mesmo tempo;
							</li>
							<li>
								Compartilhamento facilitado a custos quase insignificantes, se comprado ao custo da fotocópia ou nova plotagem do documento;
							</li>
							<li>
								Possibilidade de ter de volta o desenho em formato eletrônico e editável, de um documento que antes era exclusivamente físico, através da vetorização;
							</li>
						</ul>
						<strong class="txtblue">Estas soluções estão voltadas para digitalização de:</strong>
						<div class="row">
							<div class="col s12 m6">
								<ul class="listagem">
									<li>
										Projetos de Arquitetura e Construção Civil;
									</li>
									<li>
										Mapas;
									</li>
									<li>
										Jornais e revistas em grande formato;
									</li>
									<li>
										Desenhos de topografia e plantas planialtimétricas;
									</li>
								</ul>
							</div>
							<div class="col s12 m6">
								<ul class="listagem">
									<li>
										Desenhos de mecânica, elétrica, hidráulica e usinagem;
									</li>
									<li>
										Projetos de estruturas sólidas;
									</li>
									<li>
										Desenhos renderizados, heliográficos e em 3D.
									</li>
								</ul>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>

			<div>
				<div class="row mb40">
					<div class="col s12">
						<h3>SERVIÇOS EM AUTOCAD</h3>
						<p>
							Utilizado principalmente em arquitetura, design de interiores, engenharia civil, engenharia mecânica, engenharia geográfica, engenharia elétrica e em vários outros ramos da indústria, a Núcleo Básico possui profissionais especializados em CAD. <br>
							É utilizado para a elaboração de peças de desenho técnico em duas dimensões (2D) e para criação de modelos tridimensionais (3D). Além dos desenhos técnicos, o software disponibiliza recursos para visualização em diversos formatos. 
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>





<script>
	$('.responsiveC').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  adaptiveHeight: true
	});

</script>