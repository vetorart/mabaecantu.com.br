<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<h2 class="title tc">
				Acesso a qualquer hora ou lugar
			</h2>
			<p class="txt tc ct">
				Seus documentos, suas regras. Com base em tecnologias de Computação nas Nuvens (Cloud Computing), nossas soluções tornam seus documentos acessíveis a qualquer momento, de qualquer lugar, apenas por pessoas autorizadas, e pesquisáveis conforme você escolher. <br>
				Unindo um software inteligente e de interface intuitiva, com um serviço de armazenamento em nuvem altamente confiável, a Núcleo Básico oferece o que há de mais adequado em Armazenamento e Gerenciamento Eletrônico de Documentos.
				Descubra o imenso potencial do Gerenciamento Eletrônico de Documentos em Nuvem da Núcleo Básico para trazer benefícios para a gestão da sua empresa:
			</p>
		</div>
	</div>
</div>
<div class="blocks" id="agn">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Benefícios para sua empresa:</h3>
				<ul class="listagem">
					<li>
						Documentos acessíveis em qualquer lugar, a qualquer momento;
					</li>
					<li>
						Redução de custos com impressões desnecessárias;
					</li>
					<li>
						Controle de permissões: defina exatamente quem pode visualizar, incluir ou excluir dados do GED;
					</li>
					<li>
						Trilha de auditoria: saiba exatamente quais as ações realizadas no sistema por qualquer usuário;
					</li>
					<li>
						Indexação por infinitos campos, e indexação do conteúdo de cada documento para buscas do tipo “Full Text Search”;
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>