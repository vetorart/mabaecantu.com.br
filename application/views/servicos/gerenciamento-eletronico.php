<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<h2 class="title tc">
				Seguro e Confiável
			</h2>
			<p class="txt tc ct">
				Seus documentos, suas regras. Acessíveis a qualquer momento, de qualquer lugar, apenas por pessoas autorizadas, e pesquisáveis conforme você escolher. <br>
				Unindo um software inteligente e de interface intuitiva, com um serviço de armazenamento em nuvem altamente confiável, a Núcleo Básico oferece o que há de mais adequado em Gerenciamento Eletrônico de Documentos. <br>
				Descubra o imenso potencial do Gerenciamento Eletrônico de Documentos da Núcleo Básico para gerar benefícios para a gestão da sua empresa:
			</p>
		</div>
	</div>
</div>
<div class="blocks" id="gee">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Benefícios para sua empresa</h3>
				<ul class="listagem">
					<li>
						Documentos acessíveis em qualquer lugar, a qualquer momento;
					</li>
					<li>
						Redução de custos com impressões desnecessárias;
					</li>
					<li>
						Controle de permissões: defina exatamente quem pode visualizar, incluir ou excluir dados do GED;
					</li>
					<li>
						Trilha de auditoria: saiba exatamente quais as ações realizadas no sistema por qualquer usuário;
					</li>
					<li>
						Indexação por infinitos campos, e indexação do conteúdo para buscas do tipo “Full Text Search”;
					</li>
					<li>
						Suporte a vários formatos de arquivo eletrônico.
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>