<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<h2 class="title tc">
				Especialista na gestão de documentos
			</h2>
			<p class="txt tc ct">
				A Núcleo Básico é especialista na idealização e execução de projetos para gestão de documentos. Desenvolvemos soluções direcionadas às reais necessidades dos nossos clientes, guiados por nossa vasta experiência no tema e embasados nas melhores práticas da Arquivologia. <br> 
				Nossas soluções irão agilizar a procura por documentos e informações da sua empresa, auxiliar na redução de custos através da racionalização da produção de documentos e da otimização do espaço físico, e ainda colaborar na redução de procedimentos desnecessários.  <br>
				Entenda como desenvolvemos métodos que somam valores a todas as atividades, em especial as que dependem da informação correta, no momento em que for necessária:
			</p>
		</div>
	</div>
</div>
<div class="blocks" id="cepp">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Consultoria e Elaboração de Projetos para Gestão de Documentos</h3>
				<p>
					Contamos com profissionais com formação em Gestão de Projetos, Arquivologia, Biblioteconomia, Museologia e Gestão da Informação, capacitados para elaborar e implantar projetos de Gestão de Documentos adaptáveis a qualquer realidade, independentemente do porte e ramo da sua empresa, da natureza dos documentos, ou da localização geográfica. Dentre as principais características da Consultoria em Gestão de Documentos, estão:
				</p>
				<ul class="listagem">
					<li>
						O levantamento e análise de dados acerca da situação atual e da viabilidade do projeto;
					</li>
					<li>
						O desenvolvimento e implantação de Instrumentos Arquivísticos;
					</li>
					<li>
						Apoio na formação de Comissão Permanente de Avaliação de Documentos (CPAD);
					</li>
					<li>
						Capacitação sobre boas práticas em Gestão Documental;
					</li>
					<li>
						Orientações técnicas para adequação de ambientes que abrigam acervos documentais;
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="blocks no-margin-top no-border-top" id="doch">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Consultoria para tratamento de Documentação Histórica</h3>
				<p>
					A preservação de acervos documentais é parte da missão definida pela Núcleo Básico.  <br>
					Desenvolvendo instrumentos e aplicando práticas voltadas à conservação dos documentos que registram a história das organizações privadas e dos órgãos públicos, nós ajudamos a recuperar e preservar a trajetória de empresas e instituições, mantendo viva a sua memória. Além do nosso próprio corpo de profissionais das áreas de Arquivologia, Biblioteconomia e Museologia, contamos com PROFISSIONAIS especializados em CONSERVAÇÃO DE PAPEL, restauração de suportes danificados pela ação do tempo e que já se encontram em estágios avançados de deterioração. Dentre alguns de nosso clientes, temos: <br>Museu Paranaense, Fundação da Memória Republicana Brasileira, Iphan - Instituto do Patrimônio Histórico e Artístico Nacional, Museu Nacional de Belas Artes, Justiça Federal do Paraná, Fundação Paulo Leminski, e outros... <br>
					Dentre as principais características da Consultoria para tratamento de Documentação Histórica, estão:

				</p>
				<ul class="listagem">
					<li>
						Avaliação das condições de conservação dos documentos;
					</li>
					<li>
						Levantamento do histórico da entidade que produziu os documentos, e do período envolvido (datas-limite);
					</li>
					<li>
						Levantamento da legislação vigente nos períodos em que os documentos foram produzidos;
					</li>
					<li>
						Discussões com a entidade que produziu os documentos, para entendimento e definição do seu valor histórico;
					</li>
					<li>
						Desenvolvimento e aplicação de Instrumentos Arquivísticos básicos (Plano de Classificação, Tabela de Temporalidade);
					</li>
					<li>
						Desenvolvimento de instrumentos de pesquisa (Guias, Catálogos, Inventários, Manuais de Descrição Arquivística).
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="blocks no-margin-top no-border-top" id="poda">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Processamento e Organização de Documentos Acumulados</h3>
				<p>
					Não definir e praticar a Gestão de Documentos tem por consequência o acúmulo desnecessário e ainda compromete a localização futura. Com os conhecimentos dos nossos profissionais, planejamos métodos para resolver definitivamente estes problemas através da Consultoria em Gestão de Documentos. <br>
					Estamos também habilitados para absorver métodos já desenvolvidos e aplicá-los na identificação, classificação e organização do volume acumulado sem tratamento, encaminhando para descarte aqueles que já encerraram o seu prazo de guarda, preservando apenas o que for necessário.  <br>
					Dentre as principais características do Processamento e Organização de documentos acumulados, estão:
				</p>
				<ul class="listagem">
					<li>
						Diagnóstico inicial das condições de conservação, do período (datas-limite) e das quantidades envolvidas;
					</li>
					<li>
						Estudo do método de organização já definido;
					</li>
					<li>
						Aplicação do método de organização e prazos de guarda já definidos, com triagem do que puder ser eliminado;
					</li>
					<li>
						Destruição segura dos documentos por fragmentação assistida.
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>