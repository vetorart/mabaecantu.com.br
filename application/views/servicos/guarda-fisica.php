<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<h2 class="title tc">
				infraestrutura projetada
			</h2>
			<p class="txt tc ct">
				A Núcleo Básico possui infraestrutura com capacidade de armazenamento em contínua expansão, estrategicamente projetada para a guarda física de documentos com máxima segurança. Nossas soluções em Guarda e Gestão Física de Documentos representam a melhor alternativa para aproveitar o espaço da sua empresa na execução das atividades que realmente geram valor. <br>
				Nosso Centro de Documentação (CEDOC) oferece todas as condições necessárias para a preservação dos seus documentos, com total controle de acesso e sigilo. <br>
				Saiba o que motiva a Núcleo Básico a buscar sempre o que há de melhor para garantir segurança, conservação e acesso aos documentos dos nossos clientes de Guarda Física:
			</p>
		</div>
	</div>
</div>
<div class="blocks" id="sgf">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Segurança</h3>
				<p>
					Contamos com sistema de segurança 24h, alarme contra incêndio integrado a detectores de fumaça, sistema de contenção de chamas, além de salas de pesquisa 100% privativas para recepção de auditorias, fiscalizações, e consultas gerais. <br>
					Dentre as principais garantias de segurança com a Guarda Física de Documentos da Núcleo Básico, estão:
				</p>
				<ul class="listagem">
					<li>
						Organização sem risco de extravio das informações;
					</li>
					<li>
						Controle completo de toda a movimentação de documentos (entradas, saídas e deslocamentos para consulta), mediante protocolo.
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="blocks no-margin-top no-border-top" id="sgv">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Vantagens traduzidas em Economia para os nossos clientes</h3>
				<p>
					Mantemos nosso ambiente sempre sob condições ideais. Deixe a manutenção da infraestrutura ideal para guardar os documentos da sua empresa sob a responsabilidade da Núcleo Básico, e passe a concentrar esforços e recursos apenas no seu próprio processo produtivo. <br>
					Dentre os principais benefícios da Guarda Física de Documentos da Núcleo Básico, estão:
				</p>
				<ul class="listagem">
					<li>
						Redução de despesas para manter arquivos em local físico próprio;
					</li>
					<li>
						Economia de tempo para localizar documentos fisicamente.
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="blocks no-margin-top no-border-top" id="gfa">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<h3>Acesso onde, como e quando VOCÊ quiser</h3>
				<p>
					A Núcleo Básico ainda dispõe de um completo Serviço de Malote, que possibilita a entrega de documentos em prazos regulares e emergenciais. A logística de movimentação conta com um controle de segurança rígido, e o deslocamento é realizado usando nossos veículos próprios, destinados somente ao deslocamento seguro de suas informações. <br>
					Dentre as principais comodidades da Guarda Física de Documentos da Núcleo Básico, estão:
				</p>
				<ul class="listagem">
					<li>
						Solicitar e receber documentos fisicamente, entregues via Serviço de Malote;
					</li>
					<li>
						Consultar seus documentos presencialmente no nosso Centro de Documentação, com a privacidade oferecida pelas nossas instalações;
					</li>
					<li>
						Receber seus documentos digitalizados via e-mail, através dos nossos serviços de Digitalização Sob Demanda.
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="blocks">
	<div class="centraliza">
		<div class="slider responsiveC">
			<div>
				<div class="row">
					<div class="col s12 adicionais">
						<h4>Gestão adequada dos prazos de guarda</h4>
						<p>
							Ajustamos nossa programação para atender à Tabela de Temporalidade de Documentos da sua empresa. A Núcleo Básico fica responsável por indicar o descarte de documentos que já cumpriram o prazo de guarda definido, e realizar a destruição segura e com autorização formal do cliente.
						</p>
					</div>
				</div>
			</div>
			<div>
				<div class="row">
					<div class="col s12 adicionais">
						<h4>Incorporação de novos documentos gerados</h4>
						<p>
							Estamos preparados também para receber novos documentos gerados, mesmo após a implantação inicial da Guarda Física. Nossa capacidade de ampliação dinâmica garante todo o espaço, infraestrutura e serviços necessários, independente do ritmo de crescimento do seu arquivo. <br>
							Uma vez coletados pela equipe técnica do nosso Centro de Documentação (CEDOC), os novos documentos gerados receberão exatamente o mesmo tratamento dado àqueles que já estão sob nossos cuidados. Assim, seus arquivos continuam organizados e guardados com segurança e acesso rápido.

						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('.responsiveC').slick({
	  autoplay: true,
      autoplaySpeed: 2600,
	  dots: true,
	  infinite: true,
	  speed: 500,
	  slidesToShow: 1,
	  adaptiveHeight: true
	});

</script>