<div class="centraliza">
	<div class="row">
		<div class="col s12">
			<div class="titles uppercase">
				<span>Áreas de atuação</span>
				<h1>Direito Civil</h1>
			</div>
			<p class="txt">
				O Escritório Maba & Cantú Advocacia, por meio de seu sócio Dr. Djonathan Maba, desenvolve suas atividades profissionais voltadas ao Direito Civil, atuando em todos os seus seguimentos, inclusive o direito empresarial, direito imobiliário, contratual, marcas e patentes. <br><br>

				Dentre as atividades do núcleo da Advocacia Civil, destacam-se: 
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m6">
			<p class="subtxt uppercase barbottom">
				<span class="redtxt">Indenizações</span> (e a elaboração de defesas cabíveis)
				por danos materiais e morais: 
			</p>
			<p class="txtserv mb20 w85">
				* Visando a obtenção de indenização decorrente de atos ilícitos, danos à imagem e danos estéticos, direitos do consumidor, danos morais; <br><br>

				* Empresas: visando a obtenção de indenização decorrente de descumprimento de contratos por fornecedores e prestadores de serviços. 
			</p>
			<p class="uppercase barbottom redtxt">Ajuizamento e defesa em ações de cobranças; </p>
			<p class="uppercase barbottom redtxt">Recuperação de créditos: </p>
			<p class="txtserv mb20 w85">
				* Recuperação de créditos através de medidas administrativas e judiciais, entre estas, propositura de ações de execução, ações de cobrança, ações monitórias, ações de busca e apreensão, requerimento de falências, habilitação de créditos e impugnação de créditos declarados; <br><br>

				* Assessoria para o recebimento de créditos ou pagamento de débitos através de acordos judiciais ou extrajudiciais. 
			</p>
			<p class="uppercase barbottom redtxt">Responsabilidade Civil: </p>
			<p class="subtxt uppercase barbottom">
				<span class="redtxt">Negociações de Precatórios</span> (venda e compra de precatórios municipais, estaduais e federais); 
			</p>
			<p class="uppercase barbottom redtxt">Propositura de medidas judiciais visando à proteção de direitos civis;</p>
			<p class="uppercase barbottom redtxt">Compra e venda de títulos da dívida pública;</p>
			<p class="uppercase barbottom redtxt">Garantia de execuções com títulos públicos.</p>
			<p class="uppercase barbottom redtxt">Casamento</p>
			<p class="txtserv mb20 w85">
				- Regime de bens - Aspectos patrimoniais - Direitos e deveres dos cônjuges - Acordo pré-nupcial - Anulação de casamento.
			</p>
		</div>
		<div class="col s12 m6">
			<p class="uppercase barbottom redtxt">Código de Proteção e Defesa do Consumidor;</p>
			<p class="uppercase barbottom redtxt">Assessoria, elaboração, revisão e acompanhamento de ações de contratos civis:</p>
			<p class="txtserv mb20">
				- Contratos de compra e venda;  - Venda em consignação; - Locação; - Incorporação imobiliária; - Hipoteca; - Penhor; - Troca ou permuta; - Assistência médica;
				Consórcio; - Seguro;  -  e demais.  
			</p>
			<p class="uppercase barbottom redtxt">Separação e Divórcio</p>
			<p class="txtserv mb20">
				- Separação consensual judicial - Separação consensual cartório - Ações de separação litigiosos - Ações de divórcio consensuais - Ações de divórcio litigiosos - Dissolução do matrimônio - Guarda dos filhos - Regulamentação de visitas - Pensão alimentícia para o cônjuge - Pensão alimentícia para os filhos - Partilha do patrimônio do casal - Alimentos e execução de alimentos  
			</p>
			
			<p class="uppercase barbottom redtxt">Inventários</p>
			<p class="uppercase barbottom redtxt">Testamento</p>
			<p class="uppercase barbottom redtxt">Guarda Judicial</p>
			<p class="uppercase barbottom redtxt">Direito de Visitas</p>
			<p class="uppercase barbottom redtxt">Tutela</p>
			<p class="uppercase barbottom redtxt">Adoção</p>
			<p class="uppercase barbottom redtxt">Sucessões</p>
			<p class="uppercase barbottom redtxt">Interdição</p>
			<p class="uppercase barbottom redtxt">Investigação de Paternidade</p>
			<p class="uppercase barbottom redtxt">Separação de Corpos</p>
		</div>
	</div>
</div>