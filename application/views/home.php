<div class="centraliza mt40 mb80">
	<div class="row">
		<div class="col s12">
			<div class="titles uppercase">
				<span>Nossas Especialidades</span>
				<h1>Áreas de atuação</h1>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m4">
			<div class="blockHome">
				<strong>Direito Civil</strong>
				<div class="linet"></div>
				<p>
					O Escritório Maba & Cantú Advogados, desenvolve suas atividades profissionais voltadas ao Direito Civil, atuando em todos os seus seguimentos, inclusive o direito empresarial, direito imobiliário, contratual, marcas e patentes. 
				</p>
				<div align="center">
					<a class="btn" href="<?php echo base_url();?>direito-civil">SAIBA MAIS</a>				
				</div>
			</div>
		</div>
		<div class="col s12 m4">
			<div class="blockHome">
				<strong>Direito Criminal</strong>
				<div class="linet"></div>
				<p>
					O Escritório Maba & Cantú Advogados, desenvolve suas atividades profissionais voltadas ao Direito Criminal, atuando na defesa dos interesses de acusados ou vítimas de crimes comuns e empresariais. <br><br> 
				</p>
				<div align="center">
					<a class="btn" href="<?php echo base_url();?>direito-criminal">SAIBA MAIS</a>				
				</div>
			</div>
		</div>
		<div class="col s12 m4">
			<div class="blockHome">
				<strong>Direito Desportivo</strong>
				<div class="linet"></div>
				<p>
					O Escritório Maba & Cantú Advogados, desenvolve suas atividades profissionais de modo especializado em Direito Desportivo, atuando em todos os seus seguimentos. <br><br><br>
				</p>
				<div align="center">
					<a class="btn" href="<?php echo base_url();?>direito-desportivo">SAIBA MAIS</a>				
				</div>
			</div>
		</div>
	</div>
</div>
<div class="linecontacthome">
	<div class="centraliza">
		<div class="row">
			<div class="col s12">
				<div class="titles uppercase mb40">
					<h2>Contato</h2>
				</div>				
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<form class="formHome" action="<?php echo base_url(); ?>enviar-email" method="POST">
				<input class="badjust" type="hidden" name="urlAtual" value="<?php echo $urlAtual; ?>">
					<div class="row no-margin-bottom">
						<div class="col s12 m5">
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="nome" placeholder="NOME">
							</div>
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="email" placeholder="E-MAIL">
							</div>
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="cidade" placeholder="CIDADE">
							</div>
						</div>
						<div class="col hide-on-small-only m2">&nbsp;</div>
						<div class="col s12 m5">
							<div class="input-field no-margin-top col s12">
								<input required type="text" name="telefone" placeholder="TELEFONE">
							</div>
							<div class="input-field no-margin-top col s12">
								<textarea name="mensagem" class="materialize-textarea" placeholder="Mensagem:"></textarea>
							</div>
							<div align="center">
								<button class="btn btnblack">ENVIAR</button>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="depoimentosHome">
	<div class="slider responsiveC">
		<div>
			<div class="center740" align="center">
				"Posso não concordar com nenhuma das palavras que você disser, mas defenderei até a morte o direito de você dizê-las." <br>
				<strong class="right">Evelyn Beatrice Hall <br><br></strong>
			</div>
		</div>
		<div>
			<div class="center740" align="center">
				"Nosso dever é lutar pelo direito, mas se um dia nos depararmos entre o direito e a justiça, lutaremos pela justiça!" <br>
				<strong class="right">Eduardo Juan Couture <br><br></strong>
			</div>
		</div>
	</div>
</div>

<script>
	$('.responsiveC').slick({
	  dots: true,
	  infinite: true,
	  speed: 300,
	  slidesToShow: 1,
	  adaptiveHeight: true
	});

</script> 